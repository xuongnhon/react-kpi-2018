﻿using AutoMapper;
using BlogApp.Service.Domain;
using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using BlogApp.Service.Extensions;
using BlogApp.Service.ServiceContracts;
using Microsoft.AspNetCore.Identity;
using System.Linq;

namespace BlogApp.Service
{
    public class MappingConfiguration : Profile, IMappingConfiguration
    {
        public MappingConfiguration()
        {
            CreateMap<MenuEntity, MenuItemDto>()
                .ForMember(dest => dest.NumberOfCategories, opt => opt.MapFrom(src => src.Categories.Count))
                .ForMember(dest => dest.NumberOfPosts, opt => opt.MapFrom(src => src.Categories.Select(y => y.Posts.Count).Sum()));
            CreateMap<MenuCrudDto, MenuEntity>();


            CreateMap<CategoryEntity, CategoryItemDto>()
                .ForMember(dest => dest.MenuName, opt => opt.MapFrom(src => src.Menu.Name))
                .ForMember(dest => dest.NumberOfPosts, opt => opt.MapFrom(src => src.Posts.Count));
            CreateMap<CategoryCrudDto, CategoryEntity>();


            CreateMap<PostEntity, PostItemDto>()
               .ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => src.Category.Name))
               .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.PostTags.Select(y => y.Tag.Name).ToList()));
            CreateMap<PostCrudDto, PostEntity>();


            CreateMap<CommentEntity, CommentDto>()
                .ForMember(dest => dest.AvatarUrl, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.UserFullName, opt => opt.MapFrom(src => string.Empty))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate.GetValueOrDefault()));
            CreateMap<CommentCrudDto, CommentEntity>();


            CreateMap<SliderEntity, SliderItemDto>();
            CreateMap<SliderCrudDto, SliderEntity>();


            CreateMap<CategoryEntity, PostTrayDto>()
              .ForMember(dest => dest.CategoryId, opt => opt.MapFrom(src => src.Id))
              .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Name))
              .ForMember(dest => dest.Url, opt => opt.MapFrom(src => $"/Post/GetCategoryPostTrayDetail?categoryId={src.Id}"));
            CreateMap<PostEntity, PostTrayItemDto>()
                .ForMember(dest => dest.Url, opt => opt.MapFrom(src => $"/Post/{src.Title.ConvertToUrlFriendly()}-{src.Id}"))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate.GetValueOrDefault()));
            CreateMap<PostEntity, PostDetailDto>()
                .ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => src.Category.Name))
                .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.PostTags.Select(x => x.Tag).ToList()));
            CreateMap<TagEntity, TagItemDto>();


            CreateMap<PostEntity, PostSearchDto>()
                .ForMember(dest => dest.Link, opt => opt.MapFrom(src => $"/Post/{src.Title.ConvertToUrlFriendly()}-{src.Id}"));


            CreateMap<IdentityUser, UserItemDto>();
            CreateMap<IdentityUser, UserCrudDto>()
                .ReverseMap()
                .ForMember(dest => dest.Id, opt => opt.Ignore());
        }
    }
}
