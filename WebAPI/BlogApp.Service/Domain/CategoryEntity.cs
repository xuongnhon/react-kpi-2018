﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BlogApp.Service.Domain
{
    public class CategoryEntity : BaseEntity<int>
    {
        [MaxLength(150)]
        public string Name { get; set; }
        public string Description { get; set; }

        public int MenuId { get; set; }
        public virtual MenuEntity Menu { get; set; }

        public IList<PostEntity> Posts { get; set; }
    }
}
