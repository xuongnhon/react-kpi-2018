﻿using System.Collections.Generic;

namespace BlogApp.Service.Domain
{
    public class TagEntity : BaseEntity<int>
    {
        public string Name { get; set; }

        public IList<PostTagEntity> PostTags { get; set; }
    }
}
