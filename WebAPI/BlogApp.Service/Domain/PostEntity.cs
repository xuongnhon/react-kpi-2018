﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace BlogApp.Service.Domain
{
    public class PostEntity : BaseEntity<int>
    {
        [MaxLength(150)]
        public string Title { get; set; }
        [MaxLength(150)]
        public string ShortDescription { get; set; }
        public string Content { get; set; }
        public string ThumbnailImageUrl { get; set; }
        public int TotalView { get; set; }

        public int CategoryId { get; set; }
        public CategoryEntity Category { get; set; }

        public IList<CommentEntity> AllPostComments { get; set; }
        [NotMapped]
        public IList<CommentEntity> PostComments => AllPostComments.Where(x => x.CommentId == null).ToList();

        public IList<PostTagEntity> PostTags { get; set; }
    }
}
