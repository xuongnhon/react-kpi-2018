﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BlogApp.Service.Domain
{
    public class MenuEntity : OrderBaseEntity<int>
    {
        [MaxLength(50)]
        public string Name { get; set; }

        public IList<CategoryEntity> Categories { get; set; }
    }
}
