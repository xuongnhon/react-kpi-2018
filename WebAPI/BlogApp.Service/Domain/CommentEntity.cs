﻿using System.Collections.Generic;

namespace BlogApp.Service.Domain
{
    public class CommentEntity : BaseEntity<int>
    {
        public string Content { get; set; }

        public int PostId { get; set; }
        public PostEntity Post { get; set; }

        public int? CommentId { get; set; }
        public CommentEntity Comment { get; set; }
        public IList<CommentEntity> Comments { get; set; }
    }
}
