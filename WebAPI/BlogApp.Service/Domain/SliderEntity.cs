﻿namespace BlogApp.Service.Domain
{
    public class SliderEntity : OrderBaseEntity<int>
    {
        public string ImageUrl { get; set; }
        public string Link { get; set; }
    }
}
