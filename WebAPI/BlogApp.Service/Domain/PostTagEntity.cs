﻿namespace BlogApp.Service.Domain
{
    public class PostTagEntity : BaseEntity<int>
    {
        public int PostId { get; set; }
        public PostEntity Post { get; set; }

        public int TagId { get; set; }
        public TagEntity Tag { get; set; }
    }
}
