﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BlogApp.Service.Domain
{
    public interface IAuditEntity
    {
        string CreatedBy { get; set; }
        DateTime? CreatedDate { get; set; }
        string UpdatedBy { get; set; }
        DateTime? UpdatedDate { get; set; }
    }

    public abstract class BaseEntity<T> : IAuditEntity
    {
        [Key]
        public T Id { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }

        [Timestamp]
        public byte[] Version { get; set; }
    }

    public abstract class OrderBaseEntity<T> : BaseEntity<T>
    {
        public int Order { get; set; }
    }
}
