﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace BlogApp.Service.Utils
{
    public class TypeFinder
    {
        public IEnumerable<Type> FindClassesOfType<T>(
            IEnumerable<Assembly> assemblies,
            bool onlyConcreteClasses = true)
        {
            return FindClassesOfType(typeof(T), assemblies, onlyConcreteClasses);
        }

        public IEnumerable<Type> FindClassesOfType(
            Type assignTypeFrom,
            IEnumerable<Assembly> assemblies,
            bool onlyConcreteClasses = true
        )
        {
            var result = new List<Type>();
            try
            {
                foreach (var assembly in assemblies)
                {
                    Type[] types = null;
                    try
                    {
                        types = assembly.GetTypes();
                    }
                    catch { }

                    if (types != null)
                    {
                        foreach (var type in types)
                        {
                            if (assignTypeFrom.IsAssignableFrom(type) ||
                                (assignTypeFrom.IsGenericTypeDefinition && DoesTypeImplementOpenGeneric(type, assignTypeFrom))
                            )
                            {
                                if (!type.IsInterface)
                                {
                                    if (onlyConcreteClasses)
                                    {
                                        if (type.IsClass && !type.IsAbstract)
                                        {
                                            result.Add(type);
                                        }
                                    }
                                    else
                                    {
                                        result.Add(type);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch { }

            return result;
        }

        private bool DoesTypeImplementOpenGeneric(Type type, Type openGeneric)
        {
            try
            {
                var genericTypeDefinition = openGeneric.GetGenericTypeDefinition();
                foreach (var implementedInterface in type.FindInterfaces((objType, objCriteria) => true, null))
                {
                    if (!implementedInterface.IsGenericType)
                    {
                        continue;
                    }

                    var isMatch = genericTypeDefinition.IsAssignableFrom(implementedInterface.GetGenericTypeDefinition());

                    return isMatch;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
    }
}
