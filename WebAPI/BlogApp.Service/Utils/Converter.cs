﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace BlogApp.Service.Utils
{
    public class Converter
    {
        public static byte[] ToByteArray<T>(T obj)
        {
            if (obj == null)
            {
                return null;
            }

            var binaryFormatter = new BinaryFormatter();
            using (var memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, obj);

                return memoryStream.ToArray();
            }
        }

        public static T FromByteArray<T>(byte[] data)
        {
            if (data == null)
            {
                return default(T);
            }

            var binaryFormatter = new BinaryFormatter();
            using (var memoryStream = new MemoryStream(data))
            {
                var obj = binaryFormatter.Deserialize(memoryStream);

                return (T)obj;
            }
        }
    }
}
