﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BlogApp.Service.Caching.Impl
{
    public class DbContextStorage : IDbContextStorage, IDisposable
    {
        private readonly Dictionary<string, object> _storage = new Dictionary<string, object>();

        public T GetDbContextForKey<T>(string key) where T : class
        {
            if (!_storage.TryGetValue(key, out var context))
            {
                return null;
            }
            return context as T;
        }

        public void SetDbContextForKey<T>(string key, T context) where T : class
        {
            _storage.Add(key, context);
        }

        public IEnumerable<T> GetAllDbContexts<T>() where T : class
        {
            return _storage.Values.Select(context => context as T).ToList();
        }

        public void Dispose()
        {
            foreach (dynamic context in _storage.Values)
            {
                if (context != null)
                {
                    context.Dispose();
                }
            }
        }
    }
}
