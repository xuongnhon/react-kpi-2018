﻿using System.Collections.Generic;

namespace BlogApp.Service.Caching
{
    public interface IDbContextStorage
    {
        T GetDbContextForKey<T>(string key) where T : class;
        void SetDbContextForKey<T>(string key, T objectContext) where T : class;
        IEnumerable<T> GetAllDbContexts<T>() where T : class;
    }
}
