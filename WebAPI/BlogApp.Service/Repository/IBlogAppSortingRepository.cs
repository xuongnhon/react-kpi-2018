﻿using BlogApp.Service.Enum;
using BlogApp.Service.Context;
using BlogApp.Service.Domain;

namespace BlogApp.Service.Repository
{
    public interface IBlogAppSortingRepository<TEntity, TId> : IRepository<TEntity, TId, BlogAppDbContext>
        where TEntity : OrderBaseEntity<TId>
    {
        void Move(TEntity entity, MoveType type);
    }
}
