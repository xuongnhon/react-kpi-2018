﻿using BlogApp.Service.Enum;
using BlogApp.Service.Caching;
using BlogApp.Service.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BlogApp.Service.Repository.Impl
{
    public class Repository<TEntity, TId, TContext> : IRepository<TEntity, TId, TContext>
        where TContext : DbContext
        where TEntity : BaseEntity<TId>
    {
        private readonly IDbContextStorage _dbContextStorage;
        private readonly IServiceProvider _serviceProvider;

        public Repository(
            IDbContextStorage dbContextStorage,
            IServiceProvider serviceProvider
        )
        {
            _dbContextStorage = dbContextStorage;
            _serviceProvider = serviceProvider;
        }

        public TContext DbContext
        {
            get
            {
                var key = typeof(TContext).FullName;

                var context = _dbContextStorage.GetDbContextForKey<TContext>(key);
                if (context == null)
                {
                    context = (TContext)_serviceProvider.GetService(typeof(TContext));
                    _dbContextStorage.SetDbContextForKey(key, context);
                }

                return context;
            }
        }

        public IQueryable<TEntity> DbSet => DbContext.Set<TEntity>();

        public void Add(TEntity entity)
        {
            DbContext.Set<TEntity>().Add(entity);
        }

        public void Attach(TEntity entity, EntityState state = EntityState.Unchanged)
        {
            DbContext.Set<TEntity>().Attach(entity);
            switch (state)
            {
                case EntityState.Added:
                    DbContext.Entry(entity).State = EntityState.Added;
                    break;
                case EntityState.Deleted:
                    DbContext.Entry(entity).State = EntityState.Deleted;
                    break;
                case EntityState.Modified:
                    DbContext.Entry(entity).State = EntityState.Modified;
                    break;
                default:
                    DbContext.Entry(entity).State = EntityState.Unchanged;
                    break;
            }
        }

        public void Update(TEntity entity, Expression<Func<TEntity, bool>> criteria)
        {
            var original = FindOne(criteria);
            DbContext.Entry(original).CurrentValues.SetValues(entity);
        }

        public void Delete(TEntity entity)
        {
            DbContext.Set<TEntity>().Remove(entity);
        }

        public void Delete(Expression<Func<TEntity, bool>> criteria)
        {
            var records = DbSet.Where(criteria);

            foreach (var record in records)
            {
                Delete(record);
            }
        }

        private IQueryable<TEntity> GetQuery()
        {
            return DbContext.Set<TEntity>();
        }

        public IQueryable<TEntity> GetQuery(Expression<Func<TEntity, bool>> predicate)
        {
            return GetQuery().Where(predicate);
        }

        public TEntity GetByKey(TId keyValue)
        {
            return FindOne(x => x.Id.Equals(keyValue));
        }

        public Task<TEntity> GetByKeyAsync(TId keyValue)
        {
            return FindOneAsync(x => x.Id.Equals(keyValue));
        }

        public TEntity Single(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery().Single(criteria);
        }

        public Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery().SingleAsync(criteria);
        }

        public TEntity First()
        {
            return GetQuery().First();
        }

        public Task<TEntity> FirstAsync()
        {
            return GetQuery().FirstAsync();
        }

        public TEntity First(Expression<Func<TEntity, bool>> predicate)
        {
            return GetQuery().First(predicate);
        }

        public Task<TEntity> FirstAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return GetQuery().FirstAsync(predicate);
        }

        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery().Where(criteria);
        }

        public TEntity FindOne(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery().Where(criteria).FirstOrDefault();
        }

        public Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery().Where(criteria).FirstOrDefaultAsync();
        }

        public IQueryable<TEntity> GetAll()
        {
            return GetQuery();
        }

        public IQueryable<TEntity> Get<TOrderBy>(Expression<Func<TEntity, TOrderBy>> orderBy, int pageIndex,
            int pageSize, SortOrderType sortOrderType = SortOrderType.Ascending)
        {
            if (sortOrderType == SortOrderType.Ascending)
            {
                return GetQuery().OrderBy(orderBy).Skip((pageIndex - 1) * pageSize).Take(pageSize);
            }
            return GetQuery().OrderByDescending(orderBy).Skip((pageIndex - 1) * pageSize).Take(pageSize);
        }

        public IQueryable<TEntity> Get<TOrderBy>(Expression<Func<TEntity, bool>> criteria,
            Expression<Func<TEntity, TOrderBy>> orderBy, int pageIndex, int pageSize,
            SortOrderType sortOrderType = SortOrderType.Ascending)
        {
            if (sortOrderType == SortOrderType.Ascending)
            {
                return DbSet.Where(criteria).OrderBy(orderBy).Skip((pageIndex - 1) * pageSize).Take(pageSize);
            }
            return DbSet.Where(criteria).OrderByDescending(orderBy).Skip((pageIndex - 1) * pageSize).Take(pageSize);
        }

        public int Count()
        {
            return GetQuery().Count();
        }

        public Task<int> CountAsync()
        {
            return GetQuery().CountAsync();
        }

        public int Count(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery().Count(criteria);
        }

        public Task<int> CountAsync(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery().CountAsync(criteria);
        }
    }
}
