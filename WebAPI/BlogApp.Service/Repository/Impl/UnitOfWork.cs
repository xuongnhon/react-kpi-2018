﻿using BlogApp.Service.Caching;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace BlogApp.Service.Repository.Impl
{
    public class UnitOfWork<TContext> : IUnitOfWork
    {
        private bool _disposed;
        private readonly IDbContextStorage _dbContextStorage;

        public UnitOfWork(IDbContextStorage dbContextStorage)
        {
            _dbContextStorage = dbContextStorage;
        }

        private DbContext GetDbContext()
        {
            var contextType = typeof(TContext);
            var context = _dbContextStorage.GetDbContextForKey<DbContext>(contextType.FullName);
            if (context == null)
            {
                context = (DbContext)Activator.CreateInstance(contextType);
                _dbContextStorage.SetDbContextForKey(contextType.FullName, context);
            }

            return context;
        }

        public virtual void CommitChanges()
        {
            GetDbContext().SaveChanges();
        }

        public virtual async Task CommitChangesAsync()
        {
            await GetDbContext().SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposing)
                return;

            if (_disposed)
                return;

            _disposed = true;
        }
    }
}
