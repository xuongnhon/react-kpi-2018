﻿using BlogApp.Service.Caching;
using BlogApp.Service.Context;
using BlogApp.Service.Domain;
using System;

namespace BlogApp.Service.Repository.Impl
{
    public class BlogAppRepository<TEntity, TId> : Repository<TEntity, TId, BlogAppDbContext>, IBlogAppRepository<TEntity, TId>
        where TEntity : BaseEntity<TId>
    {
        public BlogAppRepository(
            IDbContextStorage dbContextStorage,
            IServiceProvider serviceProvider
        )
            : base(dbContextStorage, serviceProvider)
        {

        }
    }
}
