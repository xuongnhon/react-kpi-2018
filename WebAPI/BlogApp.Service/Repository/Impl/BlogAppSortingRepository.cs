﻿using BlogApp.Service.Enum;
using BlogApp.Service.Caching;
using BlogApp.Service.Context;
using BlogApp.Service.Domain;
using System;
using System.Linq;

namespace BlogApp.Service.Repository.Impl
{
    public class BlogAppSortingRepository<TEntity, TId> : Repository<TEntity, TId, BlogAppDbContext>, IBlogAppSortingRepository<TEntity, TId>
        where TEntity : OrderBaseEntity<TId>
    {
        public BlogAppSortingRepository(
            IDbContextStorage dbContextStorage,
            IServiceProvider serviceProvider
        )
            : base(dbContextStorage, serviceProvider)
        {

        }

        public void Move(TEntity entity, MoveType type)
        {
            switch (type)
            {
                case MoveType.MoveToTop:
                case MoveType.MoveToBottom:
                    var order = type == MoveType.MoveToTop
                        ? GetAll().Select(x => x.Order).Min()
                        : GetAll().Select(x => x.Order).Max();

                    entity.Order = type == MoveType.MoveToTop ? order - 1 : order + 1;
                    break;
                case MoveType.MoveUp:
                case MoveType.MoveDown:
                    var swapEntity = type == MoveType.MoveUp
                        ? Find(x => x.Order < entity.Order).OrderByDescending(x => x.Order).FirstOrDefault()
                        : Find(x => x.Order > entity.Order).OrderBy(x => x.Order).FirstOrDefault();

                    if (swapEntity != null)
                    {
                        order = entity.Order;
                        entity.Order = swapEntity.Order;
                        swapEntity.Order = order;
                    }
                    break;
            }
        }
    }
}
