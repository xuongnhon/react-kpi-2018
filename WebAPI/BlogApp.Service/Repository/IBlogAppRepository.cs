﻿using BlogApp.Service.Context;
using BlogApp.Service.Domain;

namespace BlogApp.Service.Repository
{
    public interface IBlogAppRepository<TEntity, TId> : IRepository<TEntity, TId, BlogAppDbContext>
        where TEntity : BaseEntity<TId>
    {

    }
}
