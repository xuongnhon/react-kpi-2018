﻿using BlogApp.Service.Enum;
using BlogApp.Service.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BlogApp.Service.Repository
{
    public interface IRepository<TEntity, TId, TContext>
        where TEntity : BaseEntity<TId>
    {
        TContext DbContext { get; }
        IQueryable<TEntity> DbSet { get; }

        void Add(TEntity entity);
        void Attach(TEntity entity, EntityState state = EntityState.Unchanged);
        void Update(TEntity entity, Expression<Func<TEntity, bool>> criteria);
        void Delete(TEntity entity);
        void Delete(Expression<Func<TEntity, bool>> criteria);

        IQueryable<TEntity> GetQuery(Expression<Func<TEntity, bool>> predicate);

        TEntity GetByKey(TId keyValue);
        Task<TEntity> GetByKeyAsync(TId keyValue);

        TEntity Single(Expression<Func<TEntity, bool>> criteria);
        Task<TEntity> SingleAsync(Expression<Func<TEntity, bool>> criteria);

        TEntity First();
        Task<TEntity> FirstAsync();
        TEntity First(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> FirstAsync(Expression<Func<TEntity, bool>> predicate);

        IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> criteria);

        TEntity FindOne(Expression<Func<TEntity, bool>> criteria);
        Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> criteria);

        IQueryable<TEntity> GetAll();

        IQueryable<TEntity> Get<TOrderBy>(Expression<Func<TEntity, TOrderBy>> orderBy, int pageIndex, int pageSize, SortOrderType sortOrderType = SortOrderType.Ascending);
        IQueryable<TEntity> Get<TOrderBy>(Expression<Func<TEntity, bool>> criteria, Expression<Func<TEntity, TOrderBy>> orderBy, int pageIndex, int pageSize, SortOrderType sortOrderType = SortOrderType.Ascending);

        int Count();
        Task<int> CountAsync();
        int Count(Expression<Func<TEntity, bool>> criteria);
        Task<int> CountAsync(Expression<Func<TEntity, bool>> criteria);
    }
}
