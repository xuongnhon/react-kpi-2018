﻿using System.Threading.Tasks;

namespace BlogApp.Service.Repository
{
    public interface IUnitOfWork
    {
        void CommitChanges();
        Task CommitChangesAsync();
    }
}
