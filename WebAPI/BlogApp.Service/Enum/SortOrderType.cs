﻿namespace BlogApp.Service.Enum
{
    public enum SortOrderType
    {
        Ascending = 0,
        Descending = 1
    }
}
