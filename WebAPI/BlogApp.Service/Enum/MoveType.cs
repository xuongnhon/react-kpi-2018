﻿namespace BlogApp.Service.Enum
{
    public enum MoveType
    {
        MoveToTop = 1,
        MoveUp = 2,
        MoveDown = 3,
        MoveToBottom = 4
    }
}
