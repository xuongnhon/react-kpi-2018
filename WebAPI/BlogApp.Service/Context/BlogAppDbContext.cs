﻿using BlogApp.Service.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BlogApp.Service.Context
{
    public class BlogAppDbContext : IdentityDbContext<IdentityUser>
    {
        public BlogAppDbContext(DbContextOptions<BlogAppDbContext> options)
            : base(options)
        {

        }

        public DbSet<MenuEntity> Menus { get; set; }
        public DbSet<CategoryEntity> Categories { get; set; }
        public DbSet<PostEntity> Posts { get; set; }
        public DbSet<CommentEntity> Comments { get; set; }
        public DbSet<TagEntity> Tags { get; set; }
        public DbSet<PostTagEntity> PostTags { get; set; }
        public DbSet<SliderEntity> Sliders { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<MenuEntity>(m =>
            {
                m.ToTable("Menus");
                m.HasMany(x => x.Categories).WithOne(x => x.Menu).HasForeignKey(x => x.MenuId).IsRequired();
            });

            builder.Entity<CategoryEntity>(m =>
            {
                m.ToTable("Categories");
                m.HasOne(x => x.Menu).WithMany(x => x.Categories).HasForeignKey(x => x.MenuId).IsRequired();
                m.HasMany(x => x.Posts).WithOne(x => x.Category).HasForeignKey(x => x.CategoryId).IsRequired();
            });

            builder.Entity<PostEntity>(m =>
            {
                m.ToTable("Posts");
                m.HasOne(x => x.Category).WithMany(x => x.Posts).HasForeignKey(x => x.CategoryId).IsRequired();
                m.HasMany(x => x.AllPostComments).WithOne(x => x.Post).HasForeignKey(x => x.PostId).IsRequired();
            });

            builder.Entity<CommentEntity>(m =>
            {
                m.ToTable("Comments");
                m.HasOne(x => x.Post).WithMany(x => x.AllPostComments).HasForeignKey(x => x.PostId).IsRequired();
                m.HasOne(x => x.Comment).WithMany(x => x.Comments).HasForeignKey(x => x.CommentId).IsRequired(false);
            });

            builder.Entity<PostTagEntity>(m =>
            {
                m.ToTable("PostTags");
                m.HasOne(x => x.Post).WithMany(x => x.PostTags).HasForeignKey(x => x.PostId).IsRequired();
                m.HasOne(x => x.Tag).WithMany(x => x.PostTags).HasForeignKey(x => x.TagId).IsRequired();
            });

            builder.Entity<SliderEntity>(m =>
            {
                m.ToTable("Sliders");
            });

            base.OnModelCreating(builder);
        }

        public override int SaveChanges()
        {
            AddAuditData();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            AddAuditData();
            return base.SaveChangesAsync(cancellationToken);
        }

        protected void AddAuditData()
        {
            var entities = ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditEntity && (x.State == EntityState.Added || x.State == EntityState.Modified))
                .ToList();

            if (entities.Count > 0)
            {
                var currentTime = DateTime.UtcNow;

                foreach (var entity in entities)
                {
                    var realEntity = entity.Entity as IAuditEntity;

                    if (entity.State == EntityState.Added)
                    {
                        realEntity.CreatedDate = currentTime;
                    }
                    else if (entity.State == EntityState.Modified)
                    {
                        realEntity.UpdatedDate = currentTime;
                    }
                }
            }
        }
    }
}
