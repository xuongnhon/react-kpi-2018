﻿namespace BlogApp.Service.Constants
{
    public class ErrorMessages
    {
        public const string ExcludeHtmlTags = "Invalid characters.";
        public const string InvalidUserName = "Invalid username, only letters and numbers.";
        public const string InvalidPassword = "Invalid password, sample Abc@1234.";
        public const string InvalidEmail = "Invalid email.";

        public const string NameIsRequired = "The name is required.";
        public const string DescriptionIsRequired = "The description is required.";
        public const string MenuIsRequired = "The menu is required.";
        public const string ImageUrlIsRequired = "The image url is required.";
        public const string LinkIsRequired = "The link is required.";
        public const string TitleIsRequired = "The title is required.";
        public const string ShortDescriptionIsRequired = "The short description is required.";
        public const string ContentIsRequired = "The content is required.";
        public const string ThumbnailImageUrlIsRequired = "The thumbnail image url is required.";
        public const string CategoryIsRequired = "The category is required.";
        public const string UserNameIsRequired = "The username is required.";
        public const string UserNameExist = "Username {0} is already in use.";
        public const string PasswordIsRequired = "The password is required.";
        public const string EmailIsRequired = "The email is required.";

        public const string MaxLength50 = "The maximum of characters is 50.";
        public const string MaxLength150 = "The maximum of characters is 150.";
    }
}
