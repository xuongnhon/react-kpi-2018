﻿namespace BlogApp.Service.Constants
{
    public class ConstantValues
    {
        public const string ConnectionString = "DefaultConnection";
        public const string CorsPolicy = "CorsPolicy";
        public const string WebApiHost = "WebApiHost";
        public const string IdentityUrl = "IdentityUrl";
        public const string ClientId = "ClientId";
        public const string ClientName = "ClientName";
        public const string ClientSecret = "ClientSecret";
        public const string ClientRedirectUrl = "ClientRedirectUrl";
        public const string ClientPostLogoutRedirectUrl = "ClientPostLogoutRedirectUrl";
        public const string ApiName = "ApiName";

        public const int MaximumMenu = 4;
        public const int MaximumLength15 = 15;
        public const int MaximumLength50 = 50;
        public const int MaximumLength150 = 150;

        public const string AdminImagesPath = @"images\admin";
        public const string ClientImagesPath = @"images\client";

        public const string ExcludeHtmlTagsRegex = @"^(?!((.*<(/\s*)?[^/\s])|(.*&#.*))).*(\r?\n(?!((.*<(/\s*)?[^/\s])|(.*&#.*))).*)*$";
        public const string UserNameRegex = @"^[a-zA-Z0-9]+$";
        public const string PasswordRegex = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,15}$";
        public const string EmailRegex = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";

        public const int PostTrayMaximumItem = 5;
        public const int PostTrayLoadItemLimit = 5;
        public const int SearchingMaximumItem = 5;

        public const string DateTimeFormat = "dd-MM-yyyy HH:mm:ss";
        public const string NumberFormat = "#,##0";

        public const string TokenType = "Bearer";
        public const string AccessToken = "access_token";
        public const string SessionKeyToken = "SessionKeyToken";

        public const string RoleAdmin = "admin";
        public const string AreaAdmin = "Admin";

        public const string CacheSortedMenu = "CacheSortedMenu";

        public const string Yes = "Yes";
        public const string No = "No";

        public const string ControllerUser = "User";
        public const string ActionVerifyUserName = "VerifyUserName";

        public const string ClaimsUserName = "name";
    }
}
