﻿using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.Service.ServiceContracts
{
    public interface ICategoryService : IBaseService
    {
        Task<List<CategoryItemDto>> GetAll();

        Task<ResultDto> CreateCategory(CategoryCrudDto dto);
        Task<ResultDto> EditCategory(CategoryCrudDto dto);
        Task<ResultDto> DeleteCategory(CategoryCrudDto dto);
    }
}
