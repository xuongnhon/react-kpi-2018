﻿using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.Service.ServiceContracts
{
    public interface IPostService : IBaseService
    {
        Task<KendoPagingDto> GetAll(KendoDataSourceRequestDto dto);

        Task<ResultDto> CreatePost(PostCrudDto dto);
        Task<ResultDto> EditPost(PostCrudDto dto);
        Task<ResultDto> DeletePost(PostCrudDto dto);

        Task<List<PostTrayDto>> GetCategoryPostTraiesByMenuId(int menuId);
        Task<PostTrayDetailDto> GetCategoryPostTrayDetail(int categoryId, int skip);
        Task<PostTrayDetailDto> GetLatestPostTrayDetail(int skip);

        Task<PostDetailDto> GetPostDetail(int id);

        Task<PostTrayDetailDto> GetSearchPostTrayDetail(string input, int skip);
        Task<ResultDto> SearchPosts(string input);

        Task<PostTrayDetailDto> GetTagPostTrayDetail(string tagName, int skip);
        Task<PostTrayDetailDto> GetTagPostTrayDetailByTagId(int tagId, int skip);
    }
}
