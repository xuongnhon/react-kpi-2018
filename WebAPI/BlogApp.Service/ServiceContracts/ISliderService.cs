﻿using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.Service.ServiceContracts
{
    public interface ISliderService : IBaseService
    {
        Task<List<SliderItemDto>> GetSortedSlider();

        Task<ResultDto> CreateSlider(SliderCrudDto dto);
        Task<ResultDto> EditSlider(SliderCrudDto dto);
        Task<ResultDto> DeleteSlider(SliderCrudDto dto);

        Task<ResultDto> MoveSlider(SliderCrudDto dto);
    }
}
