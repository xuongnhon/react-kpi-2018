﻿using BlogApp.Service.Dto;
using System.Threading.Tasks;

namespace BlogApp.Service.ServiceContracts
{
    public interface IUploadFileService : IBaseService
    {
        Task<FileResultDto> SaveFileForAdminSite(UploadFileDto file);
        Task<FileResultDto> SaveFileForClientSite(UploadFileDto file);
    }
}
