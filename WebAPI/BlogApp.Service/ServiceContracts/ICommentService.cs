﻿using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.Service.ServiceContracts
{
    public interface ICommentService : IBaseService
    {
        Task<List<CommentDto>> GetCommentsByPostId(int id);

        Task<ResultDto> Add(CommentCrudDto dto);
    }
}
