﻿using BlogApp.Service.Dto;
using System.Threading.Tasks;

namespace BlogApp.Service.ServiceContracts
{
    public interface ITagService : IBaseService
    {
        Task<TagItemDto> GetById(int id);
    }
}
