﻿using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.Service.ServiceContracts
{
    public interface IMenuService : IBaseService
    {
        Task<MenuItemDto> GetById(int id);
        Task<List<MenuItemDto>> GetSortedMenu();

        Task<ResultDto> CreateMenu(MenuCrudDto dto);
        Task<ResultDto> EditMenu(MenuCrudDto dto);
        Task<ResultDto> DeleteMenu(MenuCrudDto dto);

        Task<ResultDto> MoveMenu(MenuCrudDto dto);
    }
}
