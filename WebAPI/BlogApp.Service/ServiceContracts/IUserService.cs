﻿using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.Service.ServiceContracts
{
    public interface IUserService : IBaseService
    {
        Task<List<UserItemDto>> GetAll();
        Task<bool> CheckExistUserName(string userName);

        Task<ResultDto> CreateUser(UserCrudDto dto);
        Task<ResultDto> EditUser(UserCrudDto dto);
        Task<ResultDto> DeleteUser(UserCrudDto dto);
    }
}
