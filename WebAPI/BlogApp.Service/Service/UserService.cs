﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlogApp.Service.Constants;
using BlogApp.Service.Context;
using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using BlogApp.Service.Enum;
using BlogApp.Service.ServiceContracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApp.Service.Service
{
    public class UserService : IUserService
    {
        private readonly BlogAppDbContext _blogAppDbContext;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IMapper _mapper;

        public UserService(
            BlogAppDbContext blogAppDbContext,
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IMapper mapper
        )
        {
            _blogAppDbContext = blogAppDbContext;
            _userManager = userManager;
            _roleManager = roleManager;
            _mapper = mapper;
        }

        public async Task<List<UserItemDto>> GetAll()
        {
            var users = await _blogAppDbContext.Users
                .ProjectTo<UserItemDto>()
                .ToListAsync();

            var adminRole = await GetAdminRole();

            if (adminRole != null)
            {
                var adminUsers = await _blogAppDbContext.UserRoles
                    .Where(x => x.RoleId == adminRole.Id)
                    .Select(x => x.UserId)
                    .ToListAsync();

                users.ForEach(x =>
                {
                    if (adminUsers.Contains(x.Id))
                    {
                        x.IsAdmin = true;
                    }
                });
            }

            return users;
        }

        public async Task<bool> CheckExistUserName(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);

            return user != null;
        }

        public async Task<ResultDto> CreateUser(UserCrudDto dto)
        {
            var existingUser = await FindByUserName(dto.UserName);

            if (existingUser == null)
            {
                var newUser = _mapper.Map<IdentityUser>(dto);

                await _userManager.CreateAsync(newUser, dto.Password);

                if (dto.IsAdmin)
                {
                    CreateAdminRoleIfNotExist();

                    await _userManager.AddToRoleAsync(newUser, ConstantValues.RoleAdmin);
                }

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.CreateUserSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Messages.UserNameExist
            };
        }

        public async Task<ResultDto> EditUser(UserCrudDto dto)
        {
            var user = await FindById(dto.Id);

            if (user != null)
            {
                var userName = user.UserName;
                var passwordHash = _userManager.PasswordHasher.HashPassword(user, dto.Password);

                _mapper.Map(dto, user);
                user.UserName = userName;
                user.PasswordHash = passwordHash;

                await _userManager.UpdateAsync(user);

                var userRoles = await _userManager.GetRolesAsync(user);

                var haveAdminRole = userRoles.Contains(ConstantValues.RoleAdmin);

                if (dto.IsAdmin && !haveAdminRole)
                {
                    CreateAdminRoleIfNotExist();

                    await _userManager.AddToRoleAsync(user, ConstantValues.RoleAdmin);
                }

                if (!dto.IsAdmin && haveAdminRole)
                {
                    await _userManager.RemoveFromRoleAsync(user, ConstantValues.RoleAdmin);
                }

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.EditUserSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Messages.UserNotFound
            };
        }

        public async Task<ResultDto> DeleteUser(UserCrudDto dto)
        {
            var user = await FindByUserName(dto.UserName);

            if (user != null)
            {
                if (user.UserName.Equals(dto.LoggedUserName, StringComparison.OrdinalIgnoreCase))
                {
                    return new ResultDto
                    {
                        Status = ActionStatus.Error,
                        Message = Messages.UserNameLoggedIn
                    };
                }

                await _userManager.DeleteAsync(user);

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.DeleteUserSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Messages.UserNotFound
            };
        }

        private async Task<IdentityUser> FindById(string id)
        {
            return await _userManager.FindByIdAsync(id);
        }

        private async Task<IdentityUser> FindByUserName(string userName)
        {
            return await _userManager.FindByNameAsync(userName);
        }

        private async Task<IdentityRole> GetAdminRole()
        {
            return await _roleManager.FindByNameAsync(ConstantValues.RoleAdmin);
        }

        private async void CreateAdminRoleIfNotExist()
        {
            var adminRole = await GetAdminRole();

            if (adminRole == null)
            {
                await _roleManager.CreateAsync(new IdentityRole
                {
                    Name = ConstantValues.RoleAdmin
                });
            }
        }
    }
}
