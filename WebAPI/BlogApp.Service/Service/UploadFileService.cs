﻿using BlogApp.Service.Constants;
using BlogApp.Service.Dto;
using BlogApp.Service.ServiceContracts;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace BlogApp.Service.Service
{
    public class UploadFileService : IUploadFileService
    {
        private readonly List<string> _extentions = new List<string> { "png", "jpg" };
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UploadFileService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public Task<FileResultDto> SaveFileForAdminSite(UploadFileDto file)
        {
            var folderPath = $@"{WebRoot}\{ConstantValues.AdminImagesPath}";

            return Save(file, folderPath);
        }

        public Task<FileResultDto> SaveFileForClientSite(UploadFileDto file)
        {
            var folderPath = $@"{WebRoot}\{ConstantValues.ClientImagesPath}";

            return Save(file, folderPath);
        }

        private string WebRoot => $"{Directory.GetCurrentDirectory()}{@"\wwwroot"}";

        private async Task<FileResultDto> Save(UploadFileDto file, string folderPath)
        {
            var splits = Path.GetFileName(file.FileName)
                .Split('.');
            if (splits.Length > 1)
            {
                var extension = splits[splits.Length - 1].ToLower();
                if (_extentions.Contains(extension))
                {
                    var fileName = $"{Guid.NewGuid()}.{extension}";

                    var request = _httpContextAccessor.HttpContext.Request;
                    var fileUrl = $@"{request.Scheme}://{request.Host}/{ConstantValues.AdminImagesPath.Replace(@"\", "/")}/{fileName}";
                    var filePath = Path.Combine(folderPath, fileName);

                    if (file.Data.Length > 0)
                    {
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }

                        await File.WriteAllBytesAsync(filePath, file.Data);

                        return new FileResultDto
                        {
                            IsSuccess = true,
                            FileUrl = fileUrl
                        };
                    }
                }
            }

            return new FileResultDto();
        }
    }
}
