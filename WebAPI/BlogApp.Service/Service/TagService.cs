﻿using AutoMapper;
using BlogApp.Service.Domain;
using BlogApp.Service.Dto;
using BlogApp.Service.Repository;
using BlogApp.Service.ServiceContracts;
using System.Threading.Tasks;

namespace BlogApp.Service.Service
{
    public class TagService : ITagService
    {
        private readonly IBlogAppRepository<TagEntity, int> _tagRepository;
        private readonly IMapper _mapper;

        public TagService(
            IBlogAppRepository<TagEntity, int> tagRepository,
            IMapper mapper
        )
        {
            _tagRepository = tagRepository;
            _mapper = mapper;
        }

        public async Task<TagItemDto> GetById(int id)
        {
            var tag = await _tagRepository.FindOneAsync(x => x.Id == id);

            var result = _mapper.Map<TagItemDto>(tag);

            return result;
        }
    }
}
