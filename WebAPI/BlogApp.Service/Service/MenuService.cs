﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlogApp.Service.Constants;
using BlogApp.Service.Domain;
using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using BlogApp.Service.Enum;
using BlogApp.Service.Repository;
using BlogApp.Service.ServiceContracts;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApp.Service.Service
{
    public class MenuService : IMenuService
    {
        private readonly IBlogAppSortingRepository<MenuEntity, int> _menuSortingRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _uow;

        public MenuService(
            IBlogAppSortingRepository<MenuEntity, int> menuSortingRepository,
            IMapper mapper,
            IUnitOfWork uow
        )
        {
            _menuSortingRepository = menuSortingRepository;
            _mapper = mapper;
            _uow = uow;
        }

        public async Task<List<MenuItemDto>> GetSortedMenu()
        {
            var result = await _menuSortingRepository.GetAll()
                .OrderBy(x => x.Order)
                .ProjectTo<MenuItemDto>()
                .ToListAsync();

            return result;
        }

        public async Task<ResultDto> CreateMenu(MenuCrudDto dto)
        {
            var numberOfMenus = await _menuSortingRepository.CountAsync();

            if (ConstantValues.MaximumMenu > numberOfMenus)
            {
                var order = 1;
                if (numberOfMenus > 0)
                {
                    order = await _menuSortingRepository.GetAll().Select(x => x.Order).MaxAsync() + 1;
                }

                var newMenu = _mapper.Map<MenuEntity>(dto);
                newMenu.Order = order;

                _menuSortingRepository.Add(newMenu);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.CreateMenuSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Messages.LimitedMenu
            };
        }

        public async Task<ResultDto> EditMenu(MenuCrudDto dto)
        {
            var menu = await _menuSortingRepository.FindOneAsync(x => x.Id == dto.Id);
            if (menu != null)
            {
                _mapper.Map(dto, menu);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.EditMenuSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Messages.MenuNotFound
            };
        }

        public async Task<ResultDto> DeleteMenu(MenuCrudDto dto)
        {
            var menu = await _menuSortingRepository.FindOneAsync(x => x.Id == dto.Id);
            if (menu != null)
            {
                _menuSortingRepository.Delete(menu);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.DeleteMenuSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Messages.MenuNotFound
            };
        }

        public async Task<ResultDto> MoveMenu(MenuCrudDto dto)
        {
            var menu = await _menuSortingRepository.FindOneAsync(x => x.Id == dto.Id);
            if (menu != null)
            {
                _menuSortingRepository.Move(menu, dto.TypeOfMove);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.MoveMenuSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Messages.MenuNotFound
            };
        }

        public async Task<MenuItemDto> GetById(int id)
        {
            var menu = await _menuSortingRepository.FindOneAsync(x => x.Id == id);

            var result = _mapper.Map<MenuItemDto>(menu);

            return result;
        }
    }
}
