﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlogApp.Service.Constants;
using BlogApp.Service.Domain;
using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using BlogApp.Service.Enum;
using BlogApp.Service.Repository;
using BlogApp.Service.ServiceContracts;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.Service.Service
{
    public class CategoryService : ICategoryService
    {
        private readonly IBlogAppSortingRepository<MenuEntity, int> _menuSortingRepository;
        private readonly IBlogAppRepository<CategoryEntity, int> _categoryRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _uow;

        public CategoryService(
            IBlogAppSortingRepository<MenuEntity, int> menuSortingRepository,
            IBlogAppRepository<CategoryEntity, int> categoryRepository,
            IMapper mapper,
            IUnitOfWork uow
        )
        {
            _menuSortingRepository = menuSortingRepository;
            _categoryRepository = categoryRepository;
            _mapper = mapper;
            _uow = uow;
        }

        public async Task<List<CategoryItemDto>> GetAll()
        {
            var result = await _categoryRepository.GetAll()
                .ProjectTo<CategoryItemDto>()
                .ToListAsync();

            return result;
        }

        public async Task<ResultDto> CreateCategory(CategoryCrudDto dto)
        {
            var menu = await _menuSortingRepository.FindOneAsync(x => x.Id == dto.MenuId);
            if (menu != null)
            {
                var newCategory = _mapper.Map<CategoryEntity>(dto);

                _categoryRepository.Add(newCategory);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.CreateCategorySuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Messages.MenuNotFound
            };
        }

        public async Task<ResultDto> EditCategory(CategoryCrudDto dto)
        {
            var menu = await _menuSortingRepository.FindOneAsync(x => x.Id == dto.MenuId);
            if (menu != null)
            {
                var category = await _categoryRepository.FindOneAsync(x => x.Id == dto.Id);
                if (category != null)
                {
                    _mapper.Map(dto, category);

                    _categoryRepository.Attach(category, EntityState.Modified);

                    await _uow.CommitChangesAsync();

                    return new ResultDto
                    {
                        Status = ActionStatus.Success,
                        Message = Messages.EditCategorySuccessfully
                    };
                }

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.CategoryNotFound
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Messages.MenuNotFound
            };
        }

        public async Task<ResultDto> DeleteCategory(CategoryCrudDto dto)
        {
            var category = await _categoryRepository.FindOneAsync(x => x.Id == dto.Id);
            if (category != null)
            {
                _categoryRepository.Delete(category);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.DeleteCategorySuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Success,
                Message = Messages.CategoryNotFound
            };
        }
    }
}
