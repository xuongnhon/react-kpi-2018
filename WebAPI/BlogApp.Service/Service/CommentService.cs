﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlogApp.Service.Domain;
using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using BlogApp.Service.Enum;
using BlogApp.Service.Repository;
using BlogApp.Service.ServiceContracts;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.Service.Service
{
    public class CommentService : ICommentService
    {
        private readonly IBlogAppRepository<PostEntity, int> _postRepository;
        private readonly IBlogAppRepository<CommentEntity, int> _commentRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _uow;

        public CommentService(
            IBlogAppRepository<PostEntity, int> postRepository,
            IBlogAppRepository<CommentEntity, int> commentRepository,
            IMapper mapper,
            IUnitOfWork uow
        )
        {
            _postRepository = postRepository;
            _commentRepository = commentRepository;
            _mapper = mapper;
            _uow = uow;
        }

        public async Task<List<CommentDto>> GetCommentsByPostId(int id)
        {
            var result = await _commentRepository.Find(x => x.PostId == id && x.CommentId == null)
                .Include(x => x.Comments)
                .ProjectTo<CommentDto>()
                .ToListAsync();

            return result;
        }

        public async Task<ResultDto> Add(CommentCrudDto dto)
        {
            var post = await _postRepository.FindOneAsync(x => x.Id == dto.PostId);

            if (post != null)
            {
                if (dto.CommentId.HasValue)
                {
                    var comment = await _commentRepository.FindOneAsync(x => x.Id == dto.CommentId.Value
                        && x.PostId == dto.PostId
                        && x.CommentId == null);

                    if (comment == null)
                    {
                        return new ResultDto
                        {
                            Status = ActionStatus.Error
                        };
                    }
                }

                var newComment = _mapper.Map<CommentEntity>(dto);

                _commentRepository.Add(newComment);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error
            };
        }
    }
}
