﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlogApp.Service.Constants;
using BlogApp.Service.Domain;
using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using BlogApp.Service.Enum;
using BlogApp.Service.Repository;
using BlogApp.Service.ServiceContracts;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApp.Service.Service
{
    public class SliderService : ISliderService
    {
        private readonly IBlogAppSortingRepository<SliderEntity, int> _sliderSortingRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _uow;

        public SliderService(
            IBlogAppSortingRepository<SliderEntity, int> sliderSortingRepository,
            IMapper mapper,
            IUnitOfWork uow
        )
        {
            _sliderSortingRepository = sliderSortingRepository;
            _mapper = mapper;
            _uow = uow;
        }

        public async Task<List<SliderItemDto>> GetSortedSlider()
        {
            var result = await _sliderSortingRepository.GetAll()
                .OrderBy(x => x.Order)
                .ProjectTo<SliderItemDto>()
                .ToListAsync();

            return result;
        }

        public async Task<ResultDto> CreateSlider(SliderCrudDto dto)
        {
            var order = 1;
            if (await _sliderSortingRepository.CountAsync() > 0)
            {
                order = await _sliderSortingRepository.GetAll().Select(x => x.Order).MaxAsync() + 1;
            }

            var newSlider = _mapper.Map<SliderEntity>(dto);
            newSlider.Order = order;

            _sliderSortingRepository.Add(newSlider);

            await _uow.CommitChangesAsync();

            return new ResultDto
            {
                Status = ActionStatus.Success,
                Message = Messages.CreateSliderSuccessfully
            };
        }

        public async Task<ResultDto> EditSlider(SliderCrudDto dto)
        {
            var slider = await _sliderSortingRepository.FindOneAsync(x => x.Id == dto.Id);
            if (slider != null)
            {
                _mapper.Map(dto, slider);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.EditSliderSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Messages.SliderNotFound
            };
        }

        public async Task<ResultDto> DeleteSlider(SliderCrudDto dto)
        {
            var slider = await _sliderSortingRepository.FindOneAsync(x => x.Id == dto.Id);
            if (slider != null)
            {
                _sliderSortingRepository.Delete(slider);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.DeleteSliderSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Messages.SliderNotFound
            };
        }

        public async Task<ResultDto> MoveSlider(SliderCrudDto dto)
        {
            var slider = await _sliderSortingRepository.FindOneAsync(x => x.Id == dto.Id);
            if (slider != null)
            {
                _sliderSortingRepository.Move(slider, dto.TypeOfMove);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.MoveSliderSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Messages.SliderNotFound
            };
        }
    }
}
