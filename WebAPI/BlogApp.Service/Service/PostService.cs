﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using BlogApp.Service.Constants;
using BlogApp.Service.Domain;
using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using BlogApp.Service.Enum;
using BlogApp.Service.Extensions;
using BlogApp.Service.Repository;
using BlogApp.Service.ServiceContracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BlogApp.Service.Service
{
    public class PostService : IPostService
    {
        private readonly IBlogAppRepository<CategoryEntity, int> _categoryRepository;
        private readonly IBlogAppRepository<PostEntity, int> _postRepository;
        private readonly IBlogAppRepository<TagEntity, int> _tagRepository;
        private readonly IBlogAppRepository<PostTagEntity, int> _postTagRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _uow;

        public PostService(
            IBlogAppRepository<CategoryEntity, int> categoryRepository,
            IBlogAppRepository<PostEntity, int> postRepository,
            IBlogAppRepository<TagEntity, int> tagRepository,
            IBlogAppRepository<PostTagEntity, int> postTagRepository,
            IMapper mapper,
            IUnitOfWork uow
        )
        {
            _categoryRepository = categoryRepository;
            _postRepository = postRepository;
            _tagRepository = tagRepository;
            _postTagRepository = postTagRepository;
            _mapper = mapper;
            _uow = uow;
        }

        public async Task<KendoPagingDto> GetAll(KendoDataSourceRequestDto dto)
        {
            var sortDic = GetSortDictionary();
            var query = _postRepository.Find(CreateFilters(dto.Filters))
                .ProjectTo<PostItemDto>();

            if (!string.IsNullOrWhiteSpace(dto.SortField) && sortDic[dto.SortField] != null)
            {
                if (dto.SortDir.Equals("asc"))
                {
                    query = query.OrderBy(sortDic[dto.SortField]);
                }
                else
                {
                    query = query.OrderByDescending(sortDic[dto.SortField]);
                }
            }

            var result = new KendoPagingDto
            {
                Data = await query
                    .Skip(dto.Skip)
                    .Take(dto.PageSize)
                    .ToListAsync(),
                Total = await query.CountAsync()
            };

            return result;
        }

        public async Task<ResultDto> CreatePost(PostCrudDto dto)
        {
            var category = await _categoryRepository.FindOneAsync(x => x.Id == dto.CategoryId);
            if (category != null)
            {
                var newPost = _mapper.Map<PostEntity>(dto);
                newPost.PostTags = await GetTags(dto.Tags);

                _postRepository.Add(newPost);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.CreatePostSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Messages.CategoryNotFound
            };
        }

        public async Task<ResultDto> EditPost(PostCrudDto dto)
        {
            var category = await _categoryRepository.FindOneAsync(x => x.Id == dto.CategoryId);
            if (category != null)
            {
                var post = await _postRepository.Find(x => x.Id == dto.Id)
                    .Include(x => x.PostTags)
                    .FirstOrDefaultAsync();
                if (post != null)
                {
                    _mapper.Map(dto, post);
                    post.PostTags = await GetTags(dto.Tags);

                    _postRepository.Attach(post, EntityState.Modified);

                    await _uow.CommitChangesAsync();

                    return new ResultDto
                    {
                        Status = ActionStatus.Success,
                        Message = Messages.EditPostSuccessfully
                    };
                }

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.PostNotFound
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Error,
                Message = Messages.CategoryNotFound
            };
        }

        public async Task<ResultDto> DeletePost(PostCrudDto dto)
        {
            var post = await _postRepository.FindOneAsync(x => x.Id == dto.Id);
            if (post != null)
            {
                _postRepository.Delete(post);

                await _uow.CommitChangesAsync();

                return new ResultDto
                {
                    Status = ActionStatus.Success,
                    Message = Messages.DeletePostSuccessfully
                };
            }

            return new ResultDto
            {
                Status = ActionStatus.Success,
                Message = Messages.PostNotFound
            };
        }

        public async Task<List<PostTrayDto>> GetCategoryPostTraiesByMenuId(int menuId)
        {
            var categories = await _categoryRepository.Find(x => x.MenuId == menuId)
                .ToListAsync();

            var result = new List<PostTrayDto>();
            foreach (var category in categories)
            {
                var postTray = _mapper.Map<PostTrayDto>(category);

                var posts = await _postRepository.Find(x => x.CategoryId == category.Id)
                    .OrderByDescending(x => x.CreatedDate)
                    .Take(ConstantValues.PostTrayMaximumItem + 1)
                    .ToListAsync();

                postTray.Detail = new PostTrayDetailDto
                {
                    PostTrayItems = posts
                        .Select(x => _mapper.Map<PostTrayItemDto>(x))
                        .ToList()
                };

                result.Add(postTray);
            }

            result.ForEach(x =>
            {
                DiscountAndCheckIsLastItems(x.Detail);
            });

            return result;
        }

        public async Task<PostTrayDetailDto> GetCategoryPostTrayDetail(int categoryId, int skip)
        {
            var items = await _postRepository.Find(x => x.CategoryId == categoryId)
                .OrderByDescending(x => x.CreatedDate)
                .Skip(skip)
                .Take(ConstantValues.PostTrayMaximumItem + 1)
                .ProjectTo<PostTrayItemDto>()
                .ToListAsync();

            var result = new PostTrayDetailDto
            {
                PostTrayItems = items
            };

            DiscountAndCheckIsLastItems(result);

            return result;
        }

        public async Task<PostTrayDetailDto> GetLatestPostTrayDetail(int skip)
        {
            var items = await _postRepository.GetAll()
                .OrderByDescending(x => x.CreatedDate)
                .Skip(skip)
                .Take(ConstantValues.PostTrayMaximumItem + 1)
                .ProjectTo<PostTrayItemDto>()
                .ToListAsync();

            var result = new PostTrayDetailDto
            {
                PostTrayItems = items
            };

            DiscountAndCheckIsLastItems(result);

            return result;
        }

        public async Task<PostDetailDto> GetPostDetail(int id)
        {
            var post = await _postRepository.Find(x => x.Id == id)
                .Include(x => x.Category)
                .Include(x => x.PostTags)
                    .ThenInclude(x => x.Tag)
                .Include(x => x.AllPostComments)
                    .ThenInclude(x => x.Comments)
                .FirstOrDefaultAsync();
            if (post != null)
            {
                post.TotalView++;

                _postRepository.Update(post, x => x.Id == id);

                await _uow.CommitChangesAsync();

                var result = _mapper.Map<PostDetailDto>(post);

                return result;
            }

            return default(PostDetailDto);
        }

        public async Task<PostTrayDetailDto> GetSearchPostTrayDetail(string input, int skip)
        {
            var items = await _postRepository.Find(x => x.Title.ToLower().Contains(input.ToLower()))
                .OrderByDescending(x => x.CreatedDate)
                .Skip(skip)
                .Take(ConstantValues.PostTrayMaximumItem + 1)
                .ProjectTo<PostTrayItemDto>()
                .ToListAsync();

            var result = new PostTrayDetailDto
            {
                PostTrayItems = items
            };

            DiscountAndCheckIsLastItems(result);

            return result;
        }

        public async Task<ResultDto> SearchPosts(string input)
        {
            var data = await _postRepository.Find(x => x.Title.ToLower().Contains(input.ToLower()))
                .OrderByDescending(x => x.CreatedDate)
                .Take(ConstantValues.SearchingMaximumItem)
                .ProjectTo<PostSearchDto>()
                .ToListAsync();

            return new ResultDto
            {
                Status = ActionStatus.Success,
                Data = data
            };
        }

        public async Task<PostTrayDetailDto> GetTagPostTrayDetail(string tagName, int skip)
        {
            var tag = await _tagRepository.FindOneAsync(x => x.Name == tagName);

            var items = new List<PostTrayItemDto>();
            if (tag != null)
            {
                items = await _postTagRepository.Find(x => x.TagId == tag.Id)
                    .OrderByDescending(x => x.Post.CreatedDate)
                    .Select(x => x.Post)
                    .Skip(skip)
                    .Take(ConstantValues.PostTrayMaximumItem + 1)
                    .ProjectTo<PostTrayItemDto>()
                    .ToListAsync();
            }

            var result = new PostTrayDetailDto
            {
                PostTrayItems = items
            };

            DiscountAndCheckIsLastItems(result);

            return result;
        }

        public async Task<PostTrayDetailDto> GetTagPostTrayDetailByTagId(int tagId, int skip)
        {
            var tag = await _tagRepository.FindOneAsync(x => x.Id == tagId);

            var items = new List<PostTrayItemDto>();
            if (tag != null)
            {
                items = await _postTagRepository.Find(x => x.TagId == tag.Id)
                    .OrderByDescending(x => x.Post.CreatedDate)
                    .Select(x => x.Post)
                    .Skip(skip)
                    .Take(ConstantValues.PostTrayMaximumItem + 1)
                    .ProjectTo<PostTrayItemDto>()
                    .ToListAsync();
            }

            var result = new PostTrayDetailDto
            {
                PostTrayItems = items
            };

            DiscountAndCheckIsLastItems(result);

            return result;
        }

        private Expression<Func<PostEntity, bool>> CreateFilters(List<KendoDataSourceFilterDto> filters)
        {
            Expression<Func<PostEntity, bool>> expression = x => 1 == 1;

            foreach (var filter in filters)
            {
                switch (filter.Field)
                {
                    case "title":
                        expression = expression.And(x => x.Title.Contains(filter.Value));
                        break;
                    case "shortDescription":
                        expression = expression.And(x => x.ShortDescription.Contains(filter.Value));
                        break;
                    case "categoryName":
                        expression = expression.And(x => x.Category.Name.Contains(filter.Value));
                        break;
                }
            }

            return expression;
        }

        private Dictionary<string, Expression<Func<PostItemDto, object>>> GetSortDictionary()
        {
            return new Dictionary<string, Expression<Func<PostItemDto, object>>> {
                { "title", x => x.Title },
                { "shortDescription", x => x.ShortDescription },
                { "categoryName", x => x.CategoryName },
                { "totalView", x => x.TotalView }
            };
        }

        private async Task<List<PostTagEntity>> GetTags(string tags)
        {
            if (!string.IsNullOrWhiteSpace(tags))
            {
                var tagList = tags.Split(", ")
                    .Select(x => x.ToUpper())
                    .Where(x => !string.IsNullOrWhiteSpace(x))
                    .ToList();

                var existingTags = await _tagRepository.Find(x => tagList.Contains(x.Name))
                    .ToListAsync();

                var newTags = tagList.Except(existingTags.Select(x => x.Name))
                    .ToList();

                var result = existingTags
                    .Select(x => new PostTagEntity
                    {
                        TagId = x.Id
                    })
                    .Union(newTags.Select(x => new PostTagEntity
                    {
                        Tag = new TagEntity
                        {
                            Name = x.Trim().ConvertToSingleSpace()
                        }
                    }))
                    .ToList();

                return result;
            }

            return new List<PostTagEntity>();
        }

        private PostTrayDetailDto DiscountAndCheckIsLastItems(PostTrayDetailDto detail)
        {
            detail.IsLastItems = detail.PostTrayItems.Count <= ConstantValues.PostTrayMaximumItem;
            detail.PostTrayItems = detail.PostTrayItems.Take(ConstantValues.PostTrayMaximumItem).ToList();

            return detail;
        }
    }
}
