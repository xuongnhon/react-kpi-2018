﻿using System;
using System.Collections.Generic;

namespace BlogApp.Service.Dto
{
    public class CommentDto : BaseDto<int>
    {
        public string AvatarUrl { get; set; }
        public string UserFullName { get; set; }
        public string Content { get; set; }
        public DateTime CreatedDate { get; set; }

        public List<CommentDto> Comments { get; set; }
    }
}
