﻿using BlogApp.Service.Constants;
using System.ComponentModel.DataAnnotations;

namespace BlogApp.Service.Dto.Crud
{
    public class CategoryCrudDto : BaseDto<int>
    {
        [Required(ErrorMessage = ErrorMessages.NameIsRequired)]
        [MaxLength(150, ErrorMessage = ErrorMessages.MaxLength150)]
        [RegularExpression(ConstantValues.ExcludeHtmlTagsRegex, ErrorMessage = ErrorMessages.ExcludeHtmlTags)]
        public string Name { get; set; }

        [Required(ErrorMessage = ErrorMessages.DescriptionIsRequired)]
        [RegularExpression(ConstantValues.ExcludeHtmlTagsRegex, ErrorMessage = ErrorMessages.ExcludeHtmlTags)]
        public string Description { get; set; }

        [Required(ErrorMessage = ErrorMessages.MenuIsRequired)]
        public int MenuId { get; set; }
    }
}
