﻿using BlogApp.Service.Constants;
using System.ComponentModel.DataAnnotations;

namespace BlogApp.Service.Dto.Crud
{
    public class PostCrudDto : BaseDto<int>
    {
        [Required(ErrorMessage = ErrorMessages.TitleIsRequired)]
        [MaxLength(150, ErrorMessage = ErrorMessages.MaxLength150)]
        [RegularExpression(ConstantValues.ExcludeHtmlTagsRegex, ErrorMessage = ErrorMessages.ExcludeHtmlTags)]
        public string Title { get; set; }

        [Required(ErrorMessage = ErrorMessages.ShortDescriptionIsRequired)]
        [MaxLength(150, ErrorMessage = ErrorMessages.MaxLength150)]
        [RegularExpression(ConstantValues.ExcludeHtmlTagsRegex, ErrorMessage = ErrorMessages.ExcludeHtmlTags)]
        public string ShortDescription { get; set; }

        [Required(ErrorMessage = ErrorMessages.ContentIsRequired)]
        public string Content { get; set; }

        [Required(ErrorMessage = ErrorMessages.ThumbnailImageUrlIsRequired)]
        [RegularExpression(ConstantValues.ExcludeHtmlTagsRegex, ErrorMessage = ErrorMessages.ExcludeHtmlTags)]
        public string ThumbnailImageUrl { get; set; }

        [Required(ErrorMessage = ErrorMessages.CategoryIsRequired)]
        public int CategoryId { get; set; }

        [RegularExpression(ConstantValues.ExcludeHtmlTagsRegex, ErrorMessage = ErrorMessages.ExcludeHtmlTags)]
        public string Tags { get; set; }
    }
}
