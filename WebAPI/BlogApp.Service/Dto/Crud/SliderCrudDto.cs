﻿using BlogApp.Service.Constants;
using BlogApp.Service.Enum;
using System.ComponentModel.DataAnnotations;

namespace BlogApp.Service.Dto.Crud
{
    public class SliderCrudDto : BaseDto<int>
    {
        [Required(ErrorMessage = ErrorMessages.ImageUrlIsRequired)]
        [RegularExpression(ConstantValues.ExcludeHtmlTagsRegex, ErrorMessage = ErrorMessages.ExcludeHtmlTags)]
        public string ImageUrl { get; set; }

        [Required(ErrorMessage = ErrorMessages.LinkIsRequired)]
        [RegularExpression(ConstantValues.ExcludeHtmlTagsRegex, ErrorMessage = ErrorMessages.ExcludeHtmlTags)]
        public string Link { get; set; }

        public MoveType TypeOfMove { get; set; }
    }
}
