﻿using BlogApp.Service.Constants;
using BlogApp.Service.Enum;
using System.ComponentModel.DataAnnotations;

namespace BlogApp.Service.Dto.Crud
{
    public class MenuCrudDto : BaseDto<int>
    {
        [Required(ErrorMessage = ErrorMessages.NameIsRequired)]
        [MaxLength(50, ErrorMessage = ErrorMessages.MaxLength50)]
        [RegularExpression(ConstantValues.ExcludeHtmlTagsRegex, ErrorMessage = ErrorMessages.ExcludeHtmlTags)]
        public string Name { get; set; }

        public MoveType TypeOfMove { get; set; }
    }
}
