﻿using BlogApp.Service.Constants;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace BlogApp.Service.Dto.Crud
{
    public class UserCrudDto : BaseDto<string>
    {
        [Required(ErrorMessage = ErrorMessages.UserNameIsRequired)]
        [MaxLength(50, ErrorMessage = ErrorMessages.MaxLength50)]
        [RegularExpression(ConstantValues.UserNameRegex, ErrorMessage = ErrorMessages.InvalidUserName)]
        [Remote(action: ConstantValues.ActionVerifyUserName, controller: ConstantValues.ControllerUser)]
        public string UserName { get; set; }

        [Required(ErrorMessage = ErrorMessages.EmailIsRequired)]
        [MaxLength(50, ErrorMessage = ErrorMessages.MaxLength50)]
        [RegularExpression(ConstantValues.EmailRegex, ErrorMessage = ErrorMessages.InvalidEmail)]
        public string Email { get; set; }

        [Required(ErrorMessage = ErrorMessages.PasswordIsRequired)]
        [RegularExpression(ConstantValues.PasswordRegex, ErrorMessage = ErrorMessages.InvalidPassword)]
        public string Password { get; set; }

        public bool IsAdmin { get; set; }

        public string LoggedUserName { get; set; }
    }
}
