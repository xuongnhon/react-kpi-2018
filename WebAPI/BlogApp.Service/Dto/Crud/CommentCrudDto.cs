﻿using BlogApp.Service.Constants;
using System.ComponentModel.DataAnnotations;

namespace BlogApp.Service.Dto.Crud
{
    public class CommentCrudDto : BaseDto<int>
    {
        public int PostId { get; set; }

        [Required(ErrorMessage = ErrorMessages.DescriptionIsRequired)]
        [RegularExpression(ConstantValues.ExcludeHtmlTagsRegex, ErrorMessage = ErrorMessages.ExcludeHtmlTags)]
        public string Content { get; set; }

        public int? CommentId { get; set; }
    }
}
