﻿using BlogApp.Service.Constants;

namespace BlogApp.Service.Dto
{
    public class UserItemDto : BaseDto<string>
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }

        public string AdminText => IsAdmin ? ConstantValues.Yes : ConstantValues.No;
    }
}
