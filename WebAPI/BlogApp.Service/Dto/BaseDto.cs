﻿using System;

namespace BlogApp.Service.Dto
{
    [Serializable]
    public abstract class BaseDto<T>
    {
        public T Id { get; set; }
    }
}
