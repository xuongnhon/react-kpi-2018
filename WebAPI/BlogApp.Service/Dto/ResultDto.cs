﻿using BlogApp.Service.Enum;

namespace BlogApp.Service.Dto
{
    public class ResultDto
    {
        public ActionStatus Status { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
