﻿namespace BlogApp.Service.Dto
{
    public class UploadFileDto
    {
        public string FileName { get; set; }
        public byte[] Data { get; set; }
    }
}
