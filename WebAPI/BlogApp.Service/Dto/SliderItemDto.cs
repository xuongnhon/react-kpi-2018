﻿namespace BlogApp.Service.Dto
{
    public class SliderItemDto : BaseDto<int>
    {
        public string ImageUrl { get; set; }
        public string Link { get; set; }
    }
}
