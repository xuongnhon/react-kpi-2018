﻿namespace BlogApp.Service.Dto
{
    public class KendoDataSourceFilterDto
    {
        public string Field { get; set; }
        public string Value { get; set; }
    }
}
