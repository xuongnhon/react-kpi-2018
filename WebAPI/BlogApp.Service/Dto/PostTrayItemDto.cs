﻿using System;

namespace BlogApp.Service.Dto
{
    public class PostTrayItemDto : BaseDto<int>
    {
        public string Url { get; set; }
        public string ThumbnailImageUrl { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public int TotalView { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
