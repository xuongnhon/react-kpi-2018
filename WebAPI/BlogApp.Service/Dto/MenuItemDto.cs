﻿using System;

namespace BlogApp.Service.Dto
{
    [Serializable]
    public class MenuItemDto : BaseDto<int>
    {
        public string Name { get; set; }
        public int NumberOfCategories { get; set; }
        public int NumberOfPosts { get; set; }
    }
}
