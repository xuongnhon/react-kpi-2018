﻿using System.Collections.Generic;

namespace BlogApp.Service.Dto
{
    public class PostTrayDetailDto
    {
        public bool IsLastItems { get; set; }
        public List<PostTrayItemDto> PostTrayItems { get; set; }

        public PostTrayDetailDto()
        {
            PostTrayItems = new List<PostTrayItemDto>();
        }
    }
}
