﻿using System.Collections.Generic;

namespace BlogApp.Service.Dto
{
    public class PostDetailDto : BaseDto<int>
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public int TotalView { get; set; }

        public string CategoryName { get; set; }

        public List<CommentDto> PostComments { get; set; }
        public List<TagItemDto> Tags { get; set; }
    }
}
