﻿namespace BlogApp.Service.Dto
{
    public class FileResultDto
    {
        public bool IsSuccess { get; set; }
        public string FileUrl { get; set; }
    }
}
