﻿namespace BlogApp.Service.Dto
{
    public class KendoPagingDto
    {
        public object Data { get; set; }
        public int Total { get; set; }
    }
}
