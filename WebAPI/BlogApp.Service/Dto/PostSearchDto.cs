﻿namespace BlogApp.Service.Dto
{
    public class PostSearchDto : BaseDto<int>
    {
        public string Title { get; set; }
        public string ThumbnailImageUrl { get; set; }
        public string Link { get; set; }
        public int TotalView { get; set; }
    }
}
