﻿namespace BlogApp.Service.Dto
{
    public class TagItemDto : BaseDto<int>
    {
        public string Name { get; set; }
    }
}
