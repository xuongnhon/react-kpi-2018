﻿using System.Collections.Generic;

namespace BlogApp.Service.Dto
{
    public class PostItemDto : BaseDto<int>
    {
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string ThumbnailImageUrl { get; set; }
        public string Content { get; set; }
        public int TotalView { get; set; }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public List<string> Tags { get; set; }
    }
}
