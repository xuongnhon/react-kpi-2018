﻿namespace BlogApp.Service.Dto
{
    public class PostTrayDto
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public int CategoryId { get; set; }
        public PostTrayDetailDto Detail { get; set; }

        public PostTrayDto()
        {
            Detail = new PostTrayDetailDto();
        }
    }
}
