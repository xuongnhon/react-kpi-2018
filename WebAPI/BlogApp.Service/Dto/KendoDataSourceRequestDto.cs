﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.Service.Dto
{
    public class KendoDataSourceRequestDto
    {
        public int Take { get; set; }
        public int Skip { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }

        public string SortField { get; set; }
        public string SortDir { get; set; }

        public List<KendoDataSourceFilterDto> Filters { get; set; }

        public KendoDataSourceRequestDto()
        {
            Filters = new List<KendoDataSourceFilterDto>();
        }
    }

    public class KendoDataSourceRequestDtoModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var query = bindingContext.HttpContext.Request.Query;

            var result = new KendoDataSourceRequestDto
            {
                Take = GetValue<int>(query, "take"),
                Skip = GetValue<int>(query, "skip"),
                Page = GetValue<int>(query, "page"),
                PageSize = GetValue<int>(query, "pageSize"),
                SortField = GetValue<string>(query, "sort[0][field]"),
                SortDir = GetValue<string>(query, "sort[0][dir]")
            };

            var index = 0;
            while (true)
            {
                if (query.Keys.Contains($"filter[filters][{index}][field]"))
                {
                    result.Filters.Add(new KendoDataSourceFilterDto
                    {
                        Field = GetValue<string>(query, $"filter[filters][{index}][field]"),
                        Value = GetValue<string>(query, $"filter[filters][{index}][value]")
                    });
                    index++;
                }
                else
                {
                    break;
                }
            }

            bindingContext.Result = ModelBindingResult.Success(result);

            return Task.CompletedTask;
        }

        private T GetValue<T>(IQueryCollection query, string name)
        {
            StringValues value;
            if (query.TryGetValue(name, out value))
            {
                try
                {
                    return (T)Convert.ChangeType(value.ToString(), typeof(T));
                }
                catch { }
            }

            return default(T);
        }
    }
}
