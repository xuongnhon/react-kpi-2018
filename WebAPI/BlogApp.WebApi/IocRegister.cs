﻿using Autofac;
using BlogApp.Service.Utils;
using BlogApp.Service.Caching;
using BlogApp.Service.Caching.Impl;
using BlogApp.Service.Context;
using BlogApp.Service.Repository;
using BlogApp.Service.Repository.Impl;
using BlogApp.Service.ServiceContracts;
using Microsoft.AspNetCore.Http;
using System;
using System.Linq;

namespace BlogApp.WebApi
{
    public class IocRegister : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<HttpContextAccessor>().As<IHttpContextAccessor>().InstancePerLifetimeScope();

            builder.RegisterType<DbContextStorage>().As<IDbContextStorage>().InstancePerLifetimeScope();
            builder.RegisterType<UnitOfWork<BlogAppDbContext>>().As<IUnitOfWork>().InstancePerLifetimeScope();

            builder.RegisterGeneric(typeof(Repository<,,>)).As(typeof(IRepository<,,>)).InstancePerDependency();
            builder.RegisterGeneric(typeof(BlogAppRepository<,>)).As(typeof(IBlogAppRepository<,>)).InstancePerDependency();
            builder.RegisterGeneric(typeof(BlogAppSortingRepository<,>)).As(typeof(IBlogAppSortingRepository<,>)).InstancePerDependency();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var services = new TypeFinder().FindClassesOfType<IBaseService>(assemblies);
            foreach (var service in services)
            {
                var serviceInterface = service.GetInterfaces().First();
                builder.RegisterType(service).As(serviceInterface).InstancePerDependency();
            }
        }
    }
}
