﻿using AutoMapper;
using BlogApp.Service.ServiceContracts;
using BlogApp.Service.Utils;
using System;

namespace BlogApp.WebApi
{
    public class MappingRegister
    {
        public static void Register()
        {
            Mapper.Initialize(config =>
            {
                var assemblies = AppDomain.CurrentDomain.GetAssemblies();

                var mappingConfigurations = new TypeFinder().FindClassesOfType<IMappingConfiguration>(assemblies);

                foreach (var mappingConfiguration in mappingConfigurations)
                {
                    config.AddProfile(mappingConfiguration);
                }
            });
        }
    }
}
