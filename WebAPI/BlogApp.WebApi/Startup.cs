﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using BlogApp.Service.Constants;
using BlogApp.Service.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace BlogApp.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<BlogAppDbContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString(ConstantValues.ConnectionString)));

            services.AddCors(options =>
            {
                options.AddPolicy(ConstantValues.CorsPolicy,
                    policy => policy.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials());
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddAuthorization();

            services.AddAutoMapper();
            MappingRegister.Register();

            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterModule<IocRegister>();
            containerBuilder.Populate(services);
            var container = containerBuilder.Build();

            return new AutofacServiceProvider(container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();

            app.UseStaticFiles();

            app.UseCors(ConstantValues.CorsPolicy);

            app.UseMvc();
        }
    }
}
