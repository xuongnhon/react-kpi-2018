﻿using BlogApp.Service.Dto;
using BlogApp.Service.ServiceContracts;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BlogApp.WebApi.Controllers
{
    public class TagController : BaseController
    {
        private readonly ITagService _tagService;

        public TagController(ITagService tagService)
        {
            _tagService = tagService;
        }

        [HttpGet]
        public async Task<TagItemDto> GetById(int id)
        {
            var result = await _tagService.GetById(id);

            return result;
        }
    }
}
