﻿using BlogApp.Service.Constants;
using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using BlogApp.Service.ServiceContracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.WebApi.Controllers
{
    public class SliderController : BaseController
    {
        private readonly ISliderService _sliderService;

        public SliderController(ISliderService sliderService)
        {
            _sliderService = sliderService;
        }

        [HttpGet]
        public async Task<List<SliderItemDto>> GetSortedSlider()
        {
            var result = await _sliderService.GetSortedSlider();

            return result;
        }

        [HttpPost]
        [Authorize(Roles = ConstantValues.RoleAdmin)]
        public async Task<ResultDto> CreateSlider(SliderCrudDto dto)
        {
            var result = await _sliderService.CreateSlider(dto);

            return result;
        }

        [HttpPut]
        [Authorize(Roles = ConstantValues.RoleAdmin)]
        public async Task<ResultDto> EditSlider(SliderCrudDto dto)
        {
            var result = await _sliderService.EditSlider(dto);

            return result;
        }

        [HttpDelete]
        [Authorize(Roles = ConstantValues.RoleAdmin)]
        public async Task<ResultDto> DeleteSlider([FromBody] SliderCrudDto dto)
        {
            var result = await _sliderService.DeleteSlider(dto);

            return result;
        }

        [HttpPut]
        [Authorize(Roles = ConstantValues.RoleAdmin)]
        public async Task<ResultDto> MoveSlider(SliderCrudDto dto)
        {
            var result = await _sliderService.MoveSlider(dto);

            return result;
        }
    }
}
