﻿using BlogApp.Service.Constants;
using BlogApp.Service.Dto;
using BlogApp.Service.ServiceContracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BlogApp.WebApi.Controllers
{
    [Authorize(Roles = ConstantValues.RoleAdmin)]
    public class UploadController : BaseController
    {
        private readonly IUploadFileService _uploadFileService;

        public UploadController(IUploadFileService uploadFileService)
        {
            _uploadFileService = uploadFileService;
        }

        [HttpPost]
        public async Task<FileResultDto> SaveFileForAdminSite(UploadFileDto file)
        {
            var result = await _uploadFileService.SaveFileForAdminSite(file);

            return result;
        }

        [HttpPost]
        public async Task<FileResultDto> SaveFileForClientSite(UploadFileDto file)
        {
            var result = await _uploadFileService.SaveFileForClientSite(file);

            return result;
        }
    }
}
