﻿using BlogApp.Service.Constants;
using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using BlogApp.Service.ServiceContracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.WebApi.Controllers
{
    public class MenuController : BaseController
    {
        private readonly IMenuService _menuService;

        public MenuController(IMenuService menuService)
        {
            _menuService = menuService;
        }

        [HttpGet]
        public async Task<MenuItemDto> GetById(int id)
        {
            var result = await _menuService.GetById(id);

            return result;
        }

        [HttpGet]
        public async Task<List<MenuItemDto>> GetSortedMenu()
        {
            var result = await _menuService.GetSortedMenu();

            return result;
        }

        [HttpPost]
        [Authorize(Roles = ConstantValues.RoleAdmin)]
        public async Task<ResultDto> CreateMenu(MenuCrudDto dto)
        {
            var result = await _menuService.CreateMenu(dto);

            return result;
        }

        [HttpPut]
        [Authorize(Roles = ConstantValues.RoleAdmin)]
        public async Task<ResultDto> EditMenu(MenuCrudDto dto)
        {
            var result = await _menuService.EditMenu(dto);

            return result;
        }

        [HttpDelete]
        [Authorize(Roles = ConstantValues.RoleAdmin)]
        public async Task<ResultDto> DeleteMenu([FromBody]MenuCrudDto dto)
        {
            var result = await _menuService.DeleteMenu(dto);

            return result;
        }

        [HttpPut]
        [Authorize(Roles = ConstantValues.RoleAdmin)]
        public async Task<ResultDto> MoveMenu(MenuCrudDto dto)
        {
            var result = await _menuService.MoveMenu(dto);

            return result;
        }
    }
}
