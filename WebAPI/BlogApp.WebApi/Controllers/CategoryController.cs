﻿using BlogApp.Service.Constants;
using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using BlogApp.Service.ServiceContracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.WebApi.Controllers
{
    [Authorize(Roles = ConstantValues.RoleAdmin)]
    public class CategoryController : BaseController
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpGet]
        public async Task<List<CategoryItemDto>> GetAll()
        {
            var result = await _categoryService.GetAll();

            return result;
        }

        [HttpPost]
        public async Task<ResultDto> CreateCategory(CategoryCrudDto dto)
        {
            var result = await _categoryService.CreateCategory(dto);

            return result;
        }

        [HttpPut]
        public async Task<ResultDto> EditCategory(CategoryCrudDto dto)
        {
            var result = await _categoryService.EditCategory(dto);

            return result;
        }

        [HttpDelete]
        public async Task<ResultDto> DeleteCategory([FromBody] CategoryCrudDto dto)
        {
            var result = await _categoryService.DeleteCategory(dto);

            return result;
        }
    }
}
