﻿using BlogApp.Service.Constants;
using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using BlogApp.Service.ServiceContracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.WebApi.Controllers
{
    public class PostController : BaseController
    {
        private readonly IPostService _postService;

        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        [HttpGet]
        [Authorize(Roles = ConstantValues.RoleAdmin)]
        public async Task<KendoPagingDto> GetAll([FromBody]KendoDataSourceRequestDto dto)
        {
            var result = await _postService.GetAll(dto);

            return result;
        }

        [HttpPost]
        [Authorize(Roles = ConstantValues.RoleAdmin)]
        public async Task<ResultDto> CreatePost(PostCrudDto dto)
        {
            var result = await _postService.CreatePost(dto);

            return result;
        }

        [HttpPut]
        [Authorize(Roles = ConstantValues.RoleAdmin)]
        public async Task<ResultDto> EditPost(PostCrudDto dto)
        {
            var result = await _postService.EditPost(dto);

            return result;
        }

        [HttpDelete]
        [Authorize(Roles = ConstantValues.RoleAdmin)]
        public async Task<ResultDto> DeletePost([FromBody] PostCrudDto dto)
        {
            var result = await _postService.DeletePost(dto);

            return result;
        }

        [HttpGet]
        public async Task<List<PostTrayDto>> GetCategoryPostTraiesByMenuId(int menuId)
        {
            var result = await _postService.GetCategoryPostTraiesByMenuId(menuId);

            return result;
        }

        [HttpGet]
        public async Task<PostTrayDetailDto> GetCategoryPostTrayDetail(int categoryId, int skip)
        {
            var result = await _postService.GetCategoryPostTrayDetail(categoryId, skip);

            return result;
        }

        [HttpGet]
        public async Task<PostTrayDetailDto> GetLatestPostTrayDetail(int skip)
        {
            var result = await _postService.GetLatestPostTrayDetail(skip);

            return result;
        }

        [HttpGet]
        public async Task<PostDetailDto> GetPostDetail(int id)
        {
            var result = await _postService.GetPostDetail(id);

            return result;
        }

        [HttpGet]
        public async Task<PostTrayDetailDto> GetSearchPostTrayDetail(string input, int skip)
        {
            var result = await _postService.GetSearchPostTrayDetail(input, skip);

            return result;
        }

        [HttpGet]
        public async Task<ResultDto> SearchPosts(string input)
        {
            var result = await _postService.SearchPosts(input);

            return result;
        }

        [HttpGet]
        public async Task<PostTrayDetailDto> GetTagPostTrayDetail(string tagName, int skip)
        {
            var result = await _postService.GetTagPostTrayDetail(tagName, skip);

            return result;
        }

        [HttpGet]
        public async Task<PostTrayDetailDto> GetTagPostTrayDetailByTagId(int tagId, int skip)
        {
            var result = await _postService.GetTagPostTrayDetailByTagId(tagId, skip);

            return result;
        }
    }
}
