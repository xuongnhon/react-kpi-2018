﻿using BlogApp.Service.Dto;
using BlogApp.Service.Dto.Crud;
using BlogApp.Service.ServiceContracts;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlogApp.WebApi.Controllers
{
    public class CommentController : BaseController
    {
        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        [HttpGet]
        public async Task<List<CommentDto>> GetCommentsByPostId(int id)
        {
            var result = await _commentService.GetCommentsByPostId(id);

            return result;
        }

        [HttpPost]
        public async Task<ResultDto> Add(CommentCrudDto dto)
        {
            var result = await _commentService.Add(dto);

            return result;
        }
    }
}
