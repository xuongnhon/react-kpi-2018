export function convertToUrlFriendly(input) {
  return convertToSingleSpace(
    removeAllNonAlphanumericCharacters(
      convertToNonAscii(input)
    )
  ).replace(/ /g, '-');
}

export function convertToNonAscii(input) {
  const combining = /[\u0300-\u036F]/g;

  return input.normalize('NFKD').replace(combining, '')
}

export function removeAllNonAlphanumericCharacters(input) {
  return input.replace(/[^a-zA-Z0-9 -]/g, '');
}

export function convertToSingleSpace(input) {
  return input.trim().replace(/\s+/, " ");
}
