export function formatDate(date) {
  const year = date.getFullYear();

  let month = (1 + date.getMonth()).toString();

  month = month.length > 1 ? month : '0' + month;

  let day = date.getDate().toString();

  day = day.length > 1 ? day : '0' + day;

  return `${day}-${month}-${year}`;
}

export function calculateTime(date) {
  const toDay = new Date();

  const dateDiff = toDay - date;

  const seconds = dateDiff / 1000;
  const minutes = seconds / 60;
  const hours = minutes / 60;
  const days = hours / 24;

  if (seconds < 60) {
    const value = Math.max(Math.floor(seconds), 1);

    return `${value} ${value === 1 ? 'second' : 'seconds'} ago`;
  }

  if (minutes < 60) {
    const value = Math.max(Math.floor(minutes), 1);

    return `${value} ${value === 1 ? 'minute' : 'minutes'} ago`;
  }

  if (hours < 24) {
    const value = Math.max(Math.floor(hours), 1);

    return `${value} ${value === 1 ? 'hour' : 'hours'} ago`;
  }

  if (days <= 7) {
    const value = Math.max(Math.floor(days), 1);

    return `${value} ${value === 1 ? 'day' : 'days'} ago`;
  }

  return formatDate(date);
}
