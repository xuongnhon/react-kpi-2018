import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import _ from 'lodash';

import clientRoutes from './routes/client-routes';
import ClientLayout from './client/components/shared/ClientLayout';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          {
            _.map(clientRoutes, (route, key) => {
              const { component, path } = route;

              return (
                <Route
                  exact
                  path={path}
                  key={key}
                  render={(route) => <ClientLayout component={component} route={route} />}
                />
              );
            })
          }
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
