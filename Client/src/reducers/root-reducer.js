import * as Actions from '../constants/action-types';

const initialState = {
  menus: [],
  searchResultItems: [],
  sliders: [],
  postTrays: [],
  post: {
    loading: true,
    tags: [],
    commentsIsLoading: true,
    postComments: []
  }
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.RESET_STATE:
      return initialState;

    case Actions.GET_MENUS:
      action.payload();
      return state;
    case Actions.UPDATE_MENUS:
      return { ...state, menus: action.payload.menus };

    case Actions.GET_SLIDERS:
      action.payload();
      return state;
    case Actions.UPDATE_SLIDERS:
      return { ...state, sliders: action.payload.sliders };

    case Actions.SEARCH_POSTS:
      action.payload();
      return state;
    case Actions.UPDATE_SEARCH_POSTS:
      return { ...state, searchResultItems: action.payload.searchResultItems };

    case Actions.GET_POST_BY_ID:
      action.payload();
      return state;
    case Actions.UPDATE_POST:
      return { ...state, post: action.payload.post };
    case Actions.GET_COMMENTS_BY_POST_ID:
      action.payload();
      return { ...state, post: { ...state.post, commentsIsLoading: true } };
    case Actions.ADD_COMMENT:
      action.payload();
      return state;

    case Actions.GET_SEARCH_POST_TRAY:
    case Actions.GET_LATEST_POSTS:
    case Actions.GET_CATEGORY_POST_TRAYS_BY_MENU_ID:
    case Actions.GET_CATEGORY_POST_TRAY_DETAIL:
    case Actions.GET_TAG_POST_TRAY_BY_TAG_NAME:
      action.payload();
      return state;
    case Actions.UPDATE_POST_TRAYS:
      return { ...state, postTrays: action.payload.postTrays };

    default:
      return state;
  }
};

export default rootReducer;
