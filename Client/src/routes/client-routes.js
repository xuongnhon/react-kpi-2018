import Home from '../client/components/home/Home';
import Menu from '../client/components/Menu';
import Tag from '../client/components/Tag';
import Post from '../client/components/post/Post';
import Search from '../client/components/Search';

export default {
  Home: {
    component: Home,
    path: '/'
  },
  Menu: {
    component: Menu,
    path: '/Menu/:menuId/:menuName'
  },
  Tag: {
    component: Tag,
    path: '/Tag/:tagId/:tagName'
  },
  Post: {
    component: Post,
    path: '/Post/View/:postId/:postTitle'
  },
  Search: {
    component: Search,
    path: '/Post/Search/:searchValue'
  }
};
