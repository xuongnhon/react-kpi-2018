export const HOST = 'https://localhost:44359';

export const GET_MENUS_URL = `${HOST}/api/Menu/GetSortedMenu`;

export const GET_SLIDERS_URL = `${HOST}/api/Slider/GetSortedSlider`;

export const SEARCH_POSTS_URL = `${HOST}/api/Post/SearchPosts`;

export const GET_POST_BY_ID_URL = `${HOST}/api/Post/GetPostDetail`;

export const GET_COMMENTS_BY_POST_ID_URL = `${HOST}/api/Comment/GetCommentsByPostId`;

export const ADD_COMMENT_URL = `${HOST}/api/Comment/Add`;

export const GET_LATEST_POSTS_URL = `${HOST}/api/Post/GetLatestPostTrayDetail`;

export const GET_SEARCH_POST_TRAY_URL = `${HOST}/api/Post/GetSearchPostTrayDetail`;

export const GET_CATEGORY_POST_TRAYS_BY_MENU_ID_URL = `${HOST}/api/Post/GetCategoryPostTraiesByMenuId`;

export const GET_CATEGORY_POST_TRAY_DETAIL_URL = `${HOST}/api/Post/GetCategoryPostTrayDetail`;

export const GET_TAG_POST_TRAY_DETAIL_URL = `${HOST}/api/Post/GetTagPostTrayDetail`;
