export const ExcludeHtmlTagsRegex = '^(?!((.*<(/\\s*)?[^/\\s])|(.*&#.*))).*(\\r?\\n(?!((.*<(/\\s*)?[^/\\s])|(.*&#.*))).*)*$';
export const InvalidCharacters = 'Invalid characters.';
