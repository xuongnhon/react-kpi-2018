import * as Actions from '../constants/action-types';

export const resetState = () => ({
  type: Actions.RESET_STATE,
  payload: {}
});
