import store from '../store/store';
import * as Actions from '../constants/action-types';
import * as ApiUrls from '../constants/api-urls';
import { handleHttpErrors } from '../helpers/handlers';
import { updatePostTrays } from './post-actions';

export const getTagPostTrayByTagName = (tagName, skip) => ({
  type: Actions.GET_TAG_POST_TRAY_BY_TAG_NAME,
  payload: () => {
    return fetch(`${ApiUrls.GET_TAG_POST_TRAY_DETAIL_URL}?tagName=${tagName}&skip=${skip}`)
      .then(handleHttpErrors)
      .then(res => res.json())
      .then(json => {
        json.title = `Tag - ${tagName}`;

        if (skip !== 0) {
          const state = store.getState();
          const postTrays = state.postTrays;

          json.postTrayItems = [...postTrays[0].postTrayItems, ...json.postTrayItems];
        }

        store.dispatch(updatePostTrays([json]));

        return json;
      })
      .catch(error => {
        //TODO
      });
  }
});
