import store from '../store/store';
import * as Actions from '../constants/action-types';
import * as ApiUrls from '../constants/api-urls';
import { handleHttpErrors } from '../helpers/handlers';
import { updatePostTrays } from './post-actions';

export const getMenus = () => ({
  type: Actions.GET_MENUS,
  payload: () => {
    return fetch(ApiUrls.GET_MENUS_URL)
      .then(handleHttpErrors)
      .then(res => res.json())
      .then(json => {
        store.dispatch(updateMenus(json));

        return json;
      })
      .catch(error => {
        //TODO
      });
  }
});

export const updateMenus = (menus) => ({
  type: Actions.UPDATE_MENUS,
  payload: { menus }
});

export const getCategoryPostTraysByMenuId = (menuId) => ({
  type: Actions.GET_CATEGORY_POST_TRAYS_BY_MENU_ID,
  payload: () => {
    return fetch(`${ApiUrls.GET_CATEGORY_POST_TRAYS_BY_MENU_ID_URL}?menuId=${menuId}`)
      .then(handleHttpErrors)
      .then(res => res.json())
      .then(json => {
        let result = [];

        json.forEach(function (category) {
          result.push({
            title: category.title,
            isLastItems: category.detail.isLastItems,
            postTrayItems: category.detail.postTrayItems,
            categoryId: category.categoryId
          });
        });

        store.dispatch(updatePostTrays(result));

        return json;
      })
      .catch(error => {
        //TODO
      });
  }
});

export const getCategoryPostTrayDetail = (categoryId, skip) => ({
  type: Actions.GET_CATEGORY_POST_TRAY_DETAIL,
  payload: () => {
    return fetch(`${ApiUrls.GET_CATEGORY_POST_TRAY_DETAIL_URL}?categoryId=${categoryId}&skip=${skip}`)
      .then(handleHttpErrors)
      .then(res => res.json())
      .then(json => {
        const state = store.getState();
        let postTrays = [...state.postTrays];

        postTrays.forEach(function (postTray) {
          if (postTray.categoryId === categoryId) {
            postTray.isLastItems = json.isLastItems;
            postTray.postTrayItems = [...postTray.postTrayItems, ...json.postTrayItems];
          }
        });

        store.dispatch(updatePostTrays(postTrays));

        return json;
      })
      .catch(error => {
        //TODO
      });
  }
});
