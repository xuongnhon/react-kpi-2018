import store from '../store/store';
import * as Actions from '../constants/action-types';
import * as ApiUrls from '../constants/api-urls';
import * as Enums from '../constants/enums';
import { handleHttpErrors } from '../helpers/handlers';

export const searchPosts = (searchValue) => ({
  type: Actions.SEARCH_POSTS,
  payload: () => {
    const encodedValue = encodeURIComponent(searchValue);

    return fetch(`${ApiUrls.SEARCH_POSTS_URL}?input=${encodedValue}`)
      .then(handleHttpErrors)
      .then(res => res.json())
      .then(json => {
        if (json.status === Enums.ACTION_STATUS.SUCCESS) {
          store.dispatch(updateSearchPosts(json.data));
        }

        return json;
      })
      .catch(error => {
        //TODO
      });
  }
});

export const updateSearchPosts = (searchResultItems) => ({
  type: Actions.UPDATE_SEARCH_POSTS,
  payload: { searchResultItems }
});

export const getPostById = (postId) => ({
  type: Actions.GET_POST_BY_ID,
  payload: () => {
    return fetch(`${ApiUrls.GET_POST_BY_ID_URL}?id=${postId}`)
      .then(handleHttpErrors)
      .then(res => res.json())
      .then(json => {
        let state = store.getState();
        let post = state.post;

        store.dispatch(updatePost({
          ...post,
          ...json,
          loading: false,
          commentsIsLoading: false
        }));

        return json;
      })
      .catch(error => {
        //TODO
      });
  }
});

export const getCommentsByPostId = (postId, commentId) => ({
  type: Actions.GET_COMMENTS_BY_POST_ID,
  payload: () => {
    return fetch(`${ApiUrls.GET_COMMENTS_BY_POST_ID_URL}?id=${postId}`)
      .then(handleHttpErrors)
      .then(res => res.json())
      .then(json => {
        const state = store.getState();
        const post = state.post;

        json.forEach(function (comment) {
          let showReplyList = false;

          if (comment.id === commentId) {
            showReplyList = true;
          }

          comment.showReplyList = showReplyList;
        });

        store.dispatch(updatePost({
          ...post,
          commentsIsLoading: false,
          postComments: json
        }));

        return json;
      })
      .catch(error => {
        //TODO
      });
  }
});

export const updatePost = (post) => ({
  type: Actions.UPDATE_POST,
  payload: { post }
});

export const addComment = (newComment) => ({
  type: Actions.ADD_COMMENT,
  payload: () => {
    return fetch(
      ApiUrls.ADD_COMMENT_URL,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(newComment)
      })
      .then(handleHttpErrors)
      .then(res => res.json())
      .then(json => {
        if (json.status === Enums.ACTION_STATUS.SUCCESS) {
          store.dispatch(getCommentsByPostId(newComment.postId, newComment.commentId));
        }

        return json;
      })
      .catch(error => {
        //TODO
      });
  }
});

export const getLatestPosts = (skip) => ({
  type: Actions.GET_LATEST_POSTS,
  payload: () => {
    return fetch(`${ApiUrls.GET_LATEST_POSTS_URL}?skip=${skip}`)
      .then(handleHttpErrors)
      .then(res => res.json())
      .then(json => {
        json.title = 'Latest posts';

        if (skip !== 0) {
          const state = store.getState();
          const postTrays = state.postTrays;

          json.postTrayItems = [...postTrays[0].postTrayItems, ...json.postTrayItems];
        }

        store.dispatch(updatePostTrays([json]));

        return json;
      })
      .catch(error => {
        //TODO
      });
  }
});

export const getSearchPostTray = (searchValue, skip) => ({
  type: Actions.GET_SEARCH_POST_TRAY,
  payload: () => {
    const encodedValue = encodeURIComponent(searchValue);

    return fetch(`${ApiUrls.GET_SEARCH_POST_TRAY_URL}?input=${encodedValue}&skip=${skip}`)
      .then(handleHttpErrors)
      .then(res => res.json())
      .then(json => {
        json.title = `Search - ${searchValue}`;

        if (skip !== 0) {
          const state = store.getState();
          const postTrays = state.postTrays;

          json.postTrayItems = [...postTrays[0].postTrayItems, ...json.postTrayItems];
        }

        store.dispatch(updatePostTrays([json]));

        return json;
      })
      .catch(error => {
        //TODO
      });
  }
});

export const updatePostTrays = (postTrays) => ({
  type: Actions.UPDATE_POST_TRAYS,
  payload: { postTrays }
});
