import store from '../store/store';
import * as Actions from '../constants/action-types';
import * as ApiUrls from '../constants/api-urls';
import { handleHttpErrors } from '../helpers/handlers';

export const getSliders = () => ({
  type: Actions.GET_SLIDERS,
  payload: () => {
    return fetch(ApiUrls.GET_SLIDERS_URL)
      .then(handleHttpErrors)
      .then(res => res.json())
      .then(json => {
        store.dispatch(updateSliders(json));

        return json;
      })
      .catch(error => {
        //TODO
      });
  }
});

export const updateSliders = (sliders) => ({
  type: Actions.UPDATE_SLIDERS,
  payload: { sliders }
});
