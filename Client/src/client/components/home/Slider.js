import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import $ from 'jquery';
import 'jssor-slider';

const mapStateToProps = state => {
  return { sliders: state.sliders };
};

declare var $JssorSlider$;
declare var $Jease$;
declare var $JssorArrowNavigator$;
declare var $JssorBulletNavigator$;

class ConnectedSlider extends Component {
  componentDidUpdate(prevProps) {
    if (this.props.sliders.length > 0) {
      let slider = new $JssorSlider$('slider', {
        $AutoPlay: true,
        $SlideDuration: 800,
        $SlideEasing: $Jease$.$OutQuint,
        $ArrowKeyNavigation: false,
        $ArrowNavigatorOptions: {
          $Class: $JssorArrowNavigator$
        },
        $BulletNavigatorOptions: {
          $Class: $JssorBulletNavigator$
        }
      });

      function scaleSlider() {
        let refSize = slider.$Elmt.parentNode.clientWidth;

        if (refSize) {
          refSize = Math.min(refSize, 1920);

          slider.$ScaleWidth(refSize);
        } else {
          window.setTimeout(scaleSlider, 30);
        }

        slider.$Elmt.classList.add('show');
      }

      scaleSlider();

      const $window = $(window);

      $window.bind('load', scaleSlider);
      $window.bind('resize', scaleSlider);
      $window.bind('orientationchange', scaleSlider);
    }
  }

  render() {
    const sliders = this.props.sliders.map((slider, index) => {
      return (
        <div key={index} className="slider-item">
          <a href={slider.link}>
            <img data-u="image" alt="alt" src={slider.imageUrl} />
          </a>
        </div>
      );
    });

    return (
      <div className="slider-wrapper">
        {
          this.props.sliders.length > 0
            ?
            <div id="slider" className="slider">
              <div className="slider-container" data-u="slides">
                {sliders}
              </div>
              <div className="slider-navigator" data-u="navigator" data-autocenter="1">
                <div data-u="prototype"></div>
              </div>
            </div>
            : ''
        }
      </div>
    );
  }
}

const Slider = connect(mapStateToProps)(ConnectedSlider);

ConnectedSlider.propTypes = {
  sliders: PropTypes.array.isRequired
};

export default Slider;
