import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Slider from './Slider';
import ListPostTray from '../shared/ListPostTray';

import { getSliders } from '../../../actions/slider-actions'
import { getLatestPosts } from '../../../actions/post-actions'

const mapDispatchToProps = dispatch => {
  return {
    getSliders: () => dispatch(getSliders()),
    getLatestPosts: (skip) => dispatch(getLatestPosts(skip))
  };
};

class ConnectedHome extends Component {
  constructor(props) {
    super(props);

    props.getSliders();

    props.getLatestPosts(0);

    this.loadNextItems = this.loadNextItems.bind(this);
  }

  componentDidMount() {
    document.title = 'Home';
  }

  loadNextItems(postTray) {
    this.props.getLatestPosts(postTray.postTrayItems.length);
  }

  render() {
    return (
      <div>
        <Slider />

        <ListPostTray
          loadNextItems={this.loadNextItems}
        />
      </div>
    );
  }
}

const Home = connect(null, mapDispatchToProps)(ConnectedHome);

ConnectedHome.propTypes = {
  getSliders: PropTypes.func.isRequired,
  getLatestPosts: PropTypes.func.isRequired
};

export default Home;
