import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ListPostTray from './shared/ListPostTray';

import { getSearchPostTray } from '../../actions/post-actions'

const mapDispatchToProps = dispatch => {
  return {
    getSearchPostTray: (searchValue, skip) => dispatch(getSearchPostTray(searchValue, skip))
  };
};

class ConnectedSearch extends Component {
  constructor(props) {
    super(props);

    const searchValue = props.route.match.params.searchValue;

    props.getSearchPostTray(searchValue, 0);

    this.state = {
      searchValue: searchValue
    };

    this.loadNextItems = this.loadNextItems.bind(this);
  }

  componentDidMount() {
    document.title = `Search - ${this.state.searchValue}`;
  }

  loadNextItems(postTray) {
    this.props.getSearchPostTray(this.state.searchValue, postTray.postTrayItems.length);
  }

  render() {
    return (
      <div>
        <div className="height-60"></div>
        <div className="leader-board"></div>

        <ListPostTray
          loadNextItems={this.loadNextItems}
        />
      </div>
    );
  }
}

const Search = connect(null, mapDispatchToProps)(ConnectedSearch);

ConnectedSearch.propTypes = {
  getSearchPostTray: PropTypes.func.isRequired
};

export default Search;
