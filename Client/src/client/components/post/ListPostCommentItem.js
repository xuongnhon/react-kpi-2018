import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import PostCommentItem from './PostCommentItem';

const mapStateToProps = state => {
  return {
    postComments: state.post.postComments
  };
};

class ConnectedListPostCommentItem extends Component {
  constructor(props) {
    super(props);

    this.comments = [];

    this.handleReplyOnClick = this.handleReplyOnClick.bind(this);
  }

  handleReplyOnClick() {
    this.comments.forEach(function (comment) {
      comment.hideReplyInput();
    });
  }

  render() {
    const postComments = this.props.postComments.map((postComment, index) => {
      return (
        <PostCommentItem
          ref={commentRef => { this.comments[index] = commentRef }}
          key={index}
          postComment={postComment}
          handleReplyOnClick={this.handleReplyOnClick}
          addPostComment={this.props.addPostComment}
        />
      );
    });

    return (
      <div>
        {postComments}
      </div>
    );
  }
}

const ListPostCommentItem = connect(mapStateToProps)(ConnectedListPostCommentItem);

ConnectedListPostCommentItem.propTypes = {
  postComments: PropTypes.array.isRequired
};

export default ListPostCommentItem;
