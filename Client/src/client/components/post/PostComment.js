import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import * as Constants from '../../../constants/constants';
import ListPostCommentItem from './ListPostCommentItem'

const mapStateToProps = state => {
  return {
    post: state.post
  };
};

class ConnectedPostComment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      commentContent: '',
      invalidComment: false
    };

    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleKeyUp(event) {
    const commentContent = event.target.value.trim();

    if (commentContent.match(Constants.ExcludeHtmlTagsRegex) === null) {
      this.setState((state, props) => ({
        invalidComment: true
      }));

      return;
    }

    this.setState((state, props) => ({
      invalidComment: false
    }));

    if (event.key === 'Enter'
      && commentContent.length > 0
    ) {
      this.setState((state, props) => ({
        commentContent: ''
      }));

      this.props.addPostComment({
        content: commentContent
      });
    }
  }

  handleChange(event) {
    const commentContent = event.target.value;

    this.setState((state, props) => ({
      commentContent: commentContent
    }));
  }

  render() {
    return (
      <div className="player-sidebar">
        <div className="player-sidebar-header">
          Comment
        </div>
        <div className="player-sidebar-body">
          <div className="comment-input">
            <input
              type="text"
              name="comment"
              placeholder="Enter comment"
              value={this.state.commentContent}
              onChange={this.handleChange}
              onKeyUp={this.handleKeyUp}
            />
            {
              this.state.invalidComment
                ? <span class="text-color-red">{Constants.InvalidCharacters}</span>
                : ''
            }
          </div>
          <div className="comment-list">
            <div className="comment-list-wrapper">
              {
                this.props.post.commentsIsLoading
                  ? <div className="text-align-center">
                    <img src="/loading.gif" alt="loading" title="Loading" />
                  </div>
                  : <ListPostCommentItem
                    addPostComment={this.props.addPostComment}
                  />
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const PostComment = connect(mapStateToProps)(ConnectedPostComment);

ConnectedPostComment.propTypes = {
  post: PropTypes.object.isRequired
};

export default PostComment;
