import React, { Component } from 'react';
import $ from 'jquery';

import * as Constants from '../../../constants/constants';
import Timer from '../shared/Timer'
import { formatNumber } from '../../../helpers/number-helper'

class PostCommentItem extends Component {
  constructor(props) {
    super(props);

    const showReplyList = this.props.postComment.showReplyList || false;

    this.state = {
      hideReplyCount: showReplyList,
      replyListClass: showReplyList ? 'reply-list show' : 'reply-list hide',
      showReplyInput: false,
      commentContent: '',
      invalidComment: false
    };

    this.replyOnClick = this.replyOnClick.bind(this);
    this.replyCountOnClick = this.replyCountOnClick.bind(this);
    this.hideReplyInput = this.hideReplyInput.bind(this);
    this.showReplyList = this.showReplyList.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  replyOnClick() {
    if (!this.state.showReplyInput) {
      this.props.handleReplyOnClick();

      this.showReplyList();

      this.setState((state, props) => ({
        showReplyInput: true,
        commentContent: '',
        invalidComment: false
      }));

      setTimeout(() => {
        $('#replyInput').focus();
      });
    }
  }

  replyCountOnClick() {
    this.showReplyList();
  }

  hideReplyInput() {
    this.setState((state, props) => ({
      showReplyInput: false
    }));
  }

  showReplyList() {
    this.setState((state, props) => ({
      hideReplyCount: true,
      replyListClass: 'reply-list show'
    }));
  }

  handleKeyUp(event) {
    const commentContent = event.target.value.trim();

    if (commentContent.match(Constants.ExcludeHtmlTagsRegex) === null) {
      this.setState((state, props) => ({
        invalidComment: true
      }));

      return;
    }

    this.setState((state, props) => ({
      invalidComment: false
    }));

    if (event.key === 'Enter'
      && commentContent.length > 0
    ) {
      this.setState((state, props) => ({
        commentContent: ''
      }));

      this.props.addPostComment({
        content: commentContent,
        commentId: this.props.postComment.id
      });
    }
  }

  handleChange(event) {
    const commentContent = event.target.value;

    this.setState((state, props) => ({
      commentContent: commentContent
    }));
  }

  render() {
    const data = this.props.postComment;

    return (
      <div className="comment-list-item">
        <img className="author-avatar" alt="" src="/author-avatar.png" />
        <div className="comment-list-item-body">
          <div className="author-name">No name</div>
          <div className="comment-content">{data.content}</div>
          <div className="comment-action">
            <div className="comment-reply" onClick={this.replyOnClick}>
              <i className="icon-reply"></i> Reply
            </div>
            <div className="comment-time">
              <i className="icon-clock"></i> <Timer data={data.createdDate} />
            </div>
          </div>
          {
            data.comments.length > 0 && !this.state.hideReplyCount
              ? <div className="reply-count show" onClick={this.replyCountOnClick}>
                <i className="icon-level-down"></i> {formatNumber(data.comments.length)} {data.comments.length > 1 ? "replies" : "reply"}
              </div>
              : ""
          }
          <div className={this.state.replyListClass}>
            {
              data.comments.map((replyComment, index) => {
                return (
                  <div className="reply-list-item" key={index}>
                    <img className="author-avatar" alt="" src="/author-avatar.png" />
                    <div className="reply-list-item-body">
                      <div className="author-name">No name</div>
                      <div className="reply-content">{replyComment.content}</div>
                      <div className="comment-action">
                        <div className="comment-time">
                          <i className="icon-clock"></i> <Timer data={replyComment.createdDate} />
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })
            }
            {
              this.state.showReplyInput
                ? <div className="comment-input reply-input">
                  <input
                    id="replyInput"
                    type="text"
                    className="reply-input"
                    placeholder="Enter comment"
                    value={this.state.commentContent}
                    onChange={this.handleChange}
                    onKeyUp={this.handleKeyUp}
                  />
                  {
                    this.state.invalidComment
                      ? <span class="text-color-red">{Constants.InvalidCharacters}</span>
                      : ''
                  }
                </div>
                : ''
            }
          </div>
        </div>
      </div>
    );
  }
}

export default PostCommentItem;
