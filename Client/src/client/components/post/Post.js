import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import PostComment from './PostComment'
import { convertToUrlFriendly } from '../../../helpers/string-helper'
import { formatNumber } from '../../../helpers/number-helper'

import { getPostById, getCommentsByPostId, addComment } from '../../../actions/post-actions'

const mapStateToProps = state => {
  return {
    post: state.post
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPostById: (postId) => dispatch(getPostById(postId)),
    getCommentsByPostId: (postId) => dispatch(getCommentsByPostId(postId)),
    addComment: (newComment) => dispatch(addComment(newComment))
  };
};

class ConnectedPost extends Component {
  constructor(props) {
    super(props);

    const postId = Number(props.route.match.params.postId);

    props.getPostById(postId);

    this.state = {
      id: postId,
      title: props.route.match.params.postTitle
    };

    this.addPostComment = this.addPostComment.bind(this);
  }

  componentDidMount() {
    document.title = this.state.title;
  }

  componentWillReceiveProps(nextProps) {
    const postId = Number(nextProps.route.match.params.postId);

    if (postId !== this.state.id) {
      this.props.getPostById(postId);

      this.setState({
        id: postId,
        name: nextProps.route.match.params.postTitle
      });

      document.title = nextProps.route.match.params.postTitle;
    }
  }

  addPostComment(newPostComment) {
    this.props.addComment({
      postId: this.props.post.id,
      ...newPostComment
    });
  }

  render() {
    const data = this.props.post;

    return (
      <div>
        <div className="height-60"></div>
        <div className="leader-board"></div>

        <div className="player-wrapper">
          <div className="player">
            {
              !data.loading
                ?
                <div>
                  <h1 className="film-info-title">{data.title}</h1>

                  <div className="film-info-genre">
                    <div>
                      <i className="icon icon-flash"></i>
                      <span>{formatNumber(data.totalView)}</span>
                    </div>
                    <div>
                      <i className="icon icon-spin"></i>
                      <span>{data.categoryName}</span>
                    </div>
                    {
                      data.tags.length > 0
                        ? <div>
                          <i className="icon icon-list"></i>
                          {data.tags.map((tag, index) => {
                            return (
                              <Link key={index} to={`/Tag/${tag.id}/${convertToUrlFriendly(tag.name)}`}>
                                {`${tag.name}${index === data.tags.length - 1 ? '' : ', '}`}
                              </Link>
                            );
                          })}
                        </div>
                        : ''
                    }
                  </div>
                  <div className="film-info-description" dangerouslySetInnerHTML={{ __html: data.content }} />
                </div>
                : <div>&nbsp;</div>
            }
          </div>

          <PostComment
            addPostComment={this.addPostComment}
          />
        </div>
      </div>
    );
  }
}

const Post = connect(mapStateToProps, mapDispatchToProps)(ConnectedPost);

ConnectedPost.propTypes = {
  post: PropTypes.object.isRequired,
  getPostById: PropTypes.func.isRequired,
  getCommentsByPostId: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired
};

export default Post;
