import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ListPostTray from './shared/ListPostTray';

import { getTagPostTrayByTagName } from '../../actions/tag-actions'

const mapDispatchToProps = dispatch => {
  return {
    getTagPostTrayByTagName: (tagName, skip) => dispatch(getTagPostTrayByTagName(tagName, skip))
  };
};

class ConnectedTag extends Component {
  constructor(props) {
    super(props);

    const tagName = props.route.match.params.tagName;

    props.getTagPostTrayByTagName(tagName, 0);

    this.state = {
      id: props.route.match.params.tagId,
      name: tagName
    };

    this.loadNextItems = this.loadNextItems.bind(this);
  }

  componentDidMount() {
    document.title = `Tag - ${this.state.name}`;
  }

  loadNextItems(postTray) {
    this.props.getTagPostTrayByTagName(this.state.name, postTray.postTrayItems.length);
  }

  render() {
    return (
      <div>
        <div className="height-60"></div>
        <div className="leader-board"></div>

        <ListPostTray
          loadNextItems={this.loadNextItems}
        />
      </div>
    );
  }
}

const Tag = connect(null, mapDispatchToProps)(ConnectedTag);

ConnectedTag.propTypes = {
  getTagPostTrayByTagName: PropTypes.func.isRequired
};


export default Tag;
