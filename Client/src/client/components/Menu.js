import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ListPostTray from './shared/ListPostTray';

import { getCategoryPostTraysByMenuId, getCategoryPostTrayDetail } from '../../actions/menu-actions'

const mapDispatchToProps = dispatch => {
  return {
    getCategoryPostTraysByMenuId: (menuId) => dispatch(getCategoryPostTraysByMenuId(menuId)),
    getCategoryPostTrayDetail: (categoryId, skip) => dispatch(getCategoryPostTrayDetail(categoryId, skip))
  };
};

class ConnectedMenu extends Component {
  constructor(props) {
    super(props);

    const menuId = Number(props.route.match.params.menuId);

    props.getCategoryPostTraysByMenuId(menuId);

    this.state = {
      id: menuId,
      name: props.route.match.params.menuName
    };

    this.loadNextItems = this.loadNextItems.bind(this);
  }

  componentDidMount() {
    document.title = this.state.name;
  }

  componentWillReceiveProps(nextProps) {
    const menuId = Number(nextProps.route.match.params.menuId);

    if (menuId !== this.state.id) {
      this.props.getCategoryPostTraysByMenuId(menuId);

      this.setState({
        id: menuId,
        name: nextProps.route.match.params.menuName
      });

      document.title = nextProps.route.match.params.menuName;
    }
  }

  loadNextItems(postTray) {
    this.props.getCategoryPostTrayDetail(postTray.categoryId, postTray.postTrayItems.length);
  }

  render() {
    return (
      <div>
        <div className="height-60"></div>
        <div className="leader-board"></div>

        <ListPostTray
          loadNextItems={this.loadNextItems}
        />
      </div>
    );
  }
}

const Menu = connect(null, mapDispatchToProps)(ConnectedMenu);

ConnectedMenu.propTypes = {
  getCategoryPostTraysByMenuId: PropTypes.func.isRequired,
  getCategoryPostTrayDetail: PropTypes.func.isRequired
};

export default Menu;
