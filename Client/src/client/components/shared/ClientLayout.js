import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import '../../../assets/client/site.css';
import Header from './Header';
import Footer from './Footer';

import { resetState } from '../../../actions/app-actions'

const mapDispatchToProps = dispatch => {
  return {
    resetState: () => dispatch(resetState())
  };
};

class ConnectedClientLayout extends Component {
  constructor(props) {
    super(props);

    props.resetState();
  }

  render() {
    const Component = this.props.component;
    const route = this.props.route;

    return (
      <div>
        <Header />

        <div className="container">
          <Component route={route} location={route.location} />
        </div>

        <Footer />
      </div>
    );
  }
}

const ClientLayout = connect(null, mapDispatchToProps)(ConnectedClientLayout);

ConnectedClientLayout.propTypes = {
  resetState: PropTypes.func.isRequired
};

export default ClientLayout;
