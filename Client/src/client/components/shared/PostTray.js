import React, { Component } from 'react';

import PostTrayItem from './PostTrayItem';

class PostTray extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false
    };

    this.handleOnClick = this.handleOnClick.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState((state, props) => ({
      loading: false
    }));
  }

  handleOnClick() {
    this.setState((state, props) => ({
      loading: true
    }));

    this.props.loadNextItems(this.props.data);
  }

  render() {
    const postTrayItems = this.props.data.postTrayItems.map((trayItem, index) => {
      return <PostTrayItem
        key={trayItem.id}
        trayItem={trayItem}
      />;
    });

    return (
      <section className="tray">
        <div className="tray-title">
          <span>{this.props.data.title} <i className="icon-angle-right"></i></span>
        </div>
        <div className="tray-content">
          {postTrayItems}
        </div>
        {
          !this.props.data.isLastItems &&
          <div className="tray-more">
            {
              this.state.loading
                ? <img src="/loading.gif" alt="loading" title="Loading" />
                : <i
                  className="icon-down-open"
                  title="View more"
                  onClick={this.handleOnClick}
                />
            }
          </div>
        }
      </section>
    );
  }
}

export default PostTray;
