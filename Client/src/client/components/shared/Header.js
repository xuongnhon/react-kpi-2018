import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import $ from 'jquery';

import ListSearchResultItem from './ListSearchResultItem';
import { convertToUrlFriendly } from '../../../helpers/string-helper'

import { getMenus } from '../../../actions/menu-actions'
import { searchPosts, updateSearchPosts } from '../../../actions/post-actions'

const mapStateToProps = state => {
  return {
    menus: state.menus,
    searchResultItems: state.searchResultItems
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getMenus: () => dispatch(getMenus()),
    searchPosts: (searchValue) => dispatch(searchPosts(searchValue)),
    updateSearchPosts: (searchResultItems) => dispatch(updateSearchPosts(searchResultItems))
  };
};

class ConnectedHeader extends Component {
  constructor(props) {
    super(props);

    props.getMenus();

    this.activeClass = 'active';

    this.state = {
      navBarClass: 'navbar',
      navBarMenuClass: 'navbar-menu',
      navBarMenuToggleClass: 'navbar-menu-toggle',
      searchBoxClass: 'search-box',
      navBarUserClass: 'navbar-user',
      searchValue: '',
      showSearchResult: false
    };

    this.navBarMenuToggleOnClick = this.navBarMenuToggleOnClick.bind(this);
    this.navBarUserToggleOnClick = this.navBarUserToggleOnClick.bind(this);
    this.searchBoxCloseIconOnClick = this.searchBoxCloseIconOnClick.bind(this);
    this.searchBoxIconOnClick = this.searchBoxIconOnClick.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    this.handleFocus = this.handleFocus.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const component = this;

    $(document).on('mouseup', function (e) {
      const check = function (element) {
        return !element.is(e.target)
          && element.has(e.target).length === 0;
      };

      if (check($('#searchBox'))) {
        component.setState((state, props) => ({
          searchBoxClass: 'search-box'
        }));
      }

      if (check($('#searchBoxInput'))
        &&
        check($('#searchResult'))
      ) {
        component.setState((state, props) => ({
          showSearchResult: false
        }));
      }
    });
  }

  componentWillUnmount() {
    $(document).off('mouseup');
  }

  componentWillReceiveProps(nextProps) {
    this.setState((state, props) => ({
      showSearchResult: nextProps.searchResultItems.length > 0
    }));
  }

  navBarMenuToggleOnClick() {
    let activeClass = this.state.navBarMenuClass.includes(this.activeClass)
      ? ''
      : ` ${this.activeClass}`;

    this.setState((state, props) => ({
      navBarClass: `navbar${activeClass}`,
      navBarMenuClass: `navbar-menu${activeClass}`,
      searchBoxClass: 'search-box',
      navBarUserClass: 'navbar-user'
    }));
  }

  navBarUserToggleOnClick() {
    let activeClass = this.state.navBarUserClass.includes(this.activeClass)
      ? ''
      : ` ${this.activeClass}`;

    this.setState((state, props) => ({
      navBarClass: `navbar${activeClass}`,
      navBarMenuClass: 'navbar-menu',
      searchBoxClass: 'search-box',
      navBarUserClass: `navbar-user${activeClass}`
    }));
  }

  searchBoxCloseIconOnClick() {
    this.setState((state, props) => ({
      searchBoxClass: 'search-box'
    }));
  }

  searchBoxIconOnClick() {
    this.setState((state, props) => ({
      navBarClass: 'navbar',
      navBarMenuClass: 'navbar-menu',
      searchBoxClass: `search-box ${this.activeClass}`,
      navBarUserClass: 'navbar-user'
    }));
  }

  handleKeyUp(event) {
    const searchValue = event.target.value.trim();

    if (event.key === 'Enter'
      && searchValue.length > 0
    ) {
      document.location = `/Post/Search/${encodeURIComponent(searchValue)}`;
    }
  }

  handleFocus(event) {
    this.setState((state, props) => ({
      showSearchResult: this.props.searchResultItems.length > 0
    }));
  }

  handleChange(event) {
    const component = this;

    const searchValue = event.target.value;

    component.setState((state, props) => ({
      searchValue: searchValue
    }));

    if (component.searchTimeout) {
      clearTimeout(this.searchTimeout);
    }

    const trimmedSearchValue = event.target.value.trim();

    if (trimmedSearchValue.length > 0) {
      component.searchTimeout = setTimeout(() => {
        component.props.searchPosts(trimmedSearchValue);
      }, 250);
    }
    else {
      component.props.updateSearchPosts([]);
    }
  }

  render() {
    const menus = this.props.menus.map((menu, index) => {
      return (
        <Link key={index} className="navbar-menu-item" to={`/Menu/${menu.id}/${convertToUrlFriendly(menu.name)}`}>
          {menu.name}
        </Link>
      );
    });

    return (
      <header>
        <nav className={this.state.navBarClass}>
          <div className="navbar-container">
            <div className="navbar-header">
              <div className="navbar-brand">
                <Link className="logo" to="/">
                  <img src="/logo.png" alt="Blog Application"></img>
                  <span>Blog App</span>
                </Link>
              </div>

              <div className={this.state.navBarMenuClass}>
                {menus}
              </div>

              <div onClick={this.navBarMenuToggleOnClick} className={this.state.navBarMenuToggleClass}>
                <i className="icon-menu"></i>
              </div>

              <div id="searchBox" className={this.state.searchBoxClass}>
                <input
                  id="searchBoxInput"
                  type="text"
                  placeholder="Search for..."
                  autoComplete="off"
                  value={this.state.searchValue}
                  onKeyUp={this.handleKeyUp}
                  onFocus={this.handleFocus}
                  onChange={this.handleChange}
                  data-url={this.props.searchUrl}
                />

                <i onClick={this.searchBoxCloseIconOnClick} className="search-box-close icon-left-big"></i>
                <i onClick={this.searchBoxIconOnClick} className="search-box-icon icon-search"></i>
              </div>
              <div id="searchResult" className={this.state.showSearchResult && this.props.searchResultItems.length > 0 ? "navbar-dropdown search show" : "navbar-dropdown search hide"}>
                <ListSearchResultItem />
              </div>
            </div>

            <div onClick={this.navBarUserToggleOnClick}>
              <div className="navbar-user-toggle">
                <i className="icon-ellipsis-vert"></i>
              </div>
              <div className={this.state.navBarUserClass}>

              </div>
            </div>
          </div>
        </nav>
      </header>
    );
  }
}

const Header = connect(mapStateToProps, mapDispatchToProps)(ConnectedHeader);

ConnectedHeader.propTypes = {
  menus: PropTypes.array.isRequired,
  getMenus: PropTypes.func.isRequired,
  searchResultItems: PropTypes.array.isRequired,
  searchPosts: PropTypes.func.isRequired,
  updateSearchPosts: PropTypes.func.isRequired
};

export default Header;
