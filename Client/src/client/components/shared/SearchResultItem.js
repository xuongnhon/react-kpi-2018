import React from 'react';
import { Link } from 'react-router-dom';

import { convertToUrlFriendly } from '../../../helpers/string-helper'

function SearchResultItem(props) {
  const data = props.searchResultItem;

  return (
    <Link to={`/Post/View/${data.id}/${convertToUrlFriendly(data.title)}`}>
      <div className="navbar-dropdown-item">
        <div className="navbar-dropdown-item-thumbnail">
          <img src={data.thumbnailImageUrl} alt={data.title} />
        </div>
        <div className="navbar-dropdown-item-info">
          <div className="navbar-dropdown-item-title">
            {data.title}
          </div>
          <div className="navbar-dropdown-item-sub">
            {`${data.totalView} ${data.totalView > 1 ? 'views' : 'view'}`}
          </div>
        </div>
      </div>
    </Link>
  );
}

export default SearchResultItem;
