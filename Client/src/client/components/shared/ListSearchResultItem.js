import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import SearchResultItem from './SearchResultItem';

const mapStateToProps = state => {
  return {
    searchResultItems: state.searchResultItems
  };
};

function ConnectedListSearchResultItem(props) {
  const searchResultItems = props.searchResultItems.map((item, index) => {
    return (
      <SearchResultItem
        key={index}
        searchResultItem={item}
      />
    );
  });

  return (
    <div className="navbar-dropdown-body">
      {searchResultItems}
    </div>
  );
}

const ListSearchResultItem = connect(mapStateToProps)(ConnectedListSearchResultItem);

ConnectedListSearchResultItem.propTypes = {
  searchResultItems: PropTypes.array.isRequired
};

export default ListSearchResultItem;
