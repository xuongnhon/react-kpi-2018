import React, { Component } from 'react';
import $ from 'jquery';

class Footer extends Component {
  constructor(props) {
    super(props);

    this.iconUpOnClick = this.iconUpOnClick.bind(this);
  }

  iconUpOnClick() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
  }

  render() {
    return (
      <div>
        <footer>
          <div className="footer">
            <div className="footer-trend">
              <p>
                UI based on <a href="http://vuighe.net">vuighe.net</a>
              </p>
            </div>
            <div className="footer-contact">
              <p>
                Nhon Vuong
                <br />
                Email: Nhon.VuongXuong@nashtechglobal.com
              </p>
            </div>
            <div className="footer-social">
              <div className="footer-social-icon">
                <a target="_blank" rel="nofollow"><i className="icon-facebook"></i></a>
              </div>
              <div className="footer-social-icon">
                <a target="_blank" rel="nofollow"><i className="icon-youtube"></i></a>
              </div>
              <div className="footer-social-icon">
                <a target="_blank" rel="nofollow"><i className="icon-gplus"></i></a>
              </div>
              <div className="footer-social-icon">
                <a target="_blank" rel="nofollow"><i className="icon-twitter"></i></a>
              </div>
            </div>
          </div>
        </footer>
        <a onClick={this.iconUpOnClick} className="back-to-top">
          <i className="icon-up-open"></i>
        </a>
      </div>
    );
  }
}

export default Footer;
