import React from 'react';
import { Link } from 'react-router-dom';

import Timer from './Timer'
import { convertToUrlFriendly } from '../../../helpers/string-helper'
import { formatNumber } from '../../../helpers/number-helper'

function PostTrayItem(props) {
  const data = props.trayItem;

  return (
    <Link to={`/Post/View/${data.id}/${convertToUrlFriendly(data.title)}`}>
      <div className="tray-item">
        <img className="tray-item-thumbnail" src={data.thumbnailImageUrl} alt={data.title} />
        <div className="tray-item-description">
          <div className="tray-item-title">{data.title}</div>
          <div className="tray-item-meta-info">
            <span className="tray-episode-name">{data.shortDescription}</span>
            <span className="tray-episode-views">{`${formatNumber(data.totalView)} ${data.totalView > 1 ? 'views' : 'view'}`}</span>
          </div>
          <div className="tray-item-play-button">
            <span>View now</span>
          </div>
        </div>
        <div className="tray-item-bd"><Timer data={data.createdDate} /></div>
      </div>
    </Link >
  );
}

export default PostTrayItem;
