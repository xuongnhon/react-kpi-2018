import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import PostTray from './PostTray';

const mapStateToProps = state => {
  return {
    postTrays: state.postTrays
  };
};

class ConnectedListPostTray extends Component {
  render() {
    const postTrays = this.props.postTrays.map((postTray, index) => {
      return (
        <PostTray
          key={index}
          data={postTray}
          loadNextItems={this.props.loadNextItems}
        />
      );
    });

    return (
      <div>
        {postTrays}
      </div>
    );
  }
}

const ListPostTray = connect(mapStateToProps)(ConnectedListPostTray);

ConnectedListPostTray.propTypes = {
  postTrays: PropTypes.array.isRequired
};

export default ListPostTray;
