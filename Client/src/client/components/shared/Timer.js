import React, { Component } from 'react';

import * as DateHelper from '../../../helpers/date-helper'

class Timer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      updateCount: 0
    };
  }

  componentDidMount() {
    const component = this;

    setInterval(function () {
      component.setState((state, props) => ({
        updateCount: state.updateCount + 1
      }));
    }, 60000);
  }

  render() {
    const data = DateHelper.calculateTime(
      new Date(this.props.data)
    );

    return (
      <span>{data}</span>
    );
  }
}

export default Timer;
